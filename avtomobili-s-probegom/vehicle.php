<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Автомобиль");
?><? $APPLICATION->IncludeComponent(
    "maxposter:api.vehicle",
    "dolcar",
    array(
        "ADD_SECTIONS_CHAIN" => "Y",
        "COMPONENT_TEMPLATE" => "dolcar",
        "MAX_API_LOGIN" => "267",
        "MAX_API_PASSWORD" => "dolauto267",
        "SET_STATUS_404" => "N",
        "SET_TITLE" => "Y",
        "SHOW_ADRESS" => "Y",
        "SHOW_AVAILABILITY" => "N",
        "SHOW_COLOR" => "Y",
        "SHOW_CONDITION" => "Y",
        "SHOW_CONTACTS" => "N",
        "SHOW_CUSTOMS" => "N",
        "SHOW_DESCRIPTION" => "Y",
        "SHOW_INSPECTION" => "N",
        "SHOW_MARK_MODEL" => "Y",
        "SHOW_PTS" => "Y",
        "URL_TEMPLATES_INDEX" => "/avtomobili-s-probegom/",
        "URL_TEMPLATES_VEHICLE" => "/avtomobili-s-probegom/vehicle.php?VEHICLE_ID=#VEHICLE_ID#",
        "VEHICLE_ID" => $_REQUEST["VEHICLE_ID"]
    ),
    false
); ?>

    <div class="similar">
        <div class="row service-bottom">
            <div class="col-md-3 col-sm-6 services">
                <div class="inner-block">
                    <a class="link-services" href="/sale">
                        <div class="ico-services buyout">
                        </div>
                        <h4>Выкуп</h4>
                        Быстрый и легкий способ продать автомобиль за 1 час. </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 services">
                <div class="inner-block">
                    <a href="/service/trade-in" class="link-services">
                        <div class="ico-services trade-in">
                        </div>
                        <h4>Трейд-ин</h4>
                        Удобный способ обменять свой автомобиль. </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 services">
                <div class="inner-block">
                    <a href="/uslugi/kreditovanie" class="link-services">
                        <div class="ico-services credit">
                        </div>
                        <h4>Кредит</h4>
                        Подберем кредит на вгодных индивидуальных условиях. </a>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 services">
                <div class="inner-block">
                    <a href="/sale" class="link-services">
                        <div class="ico-services exchange">
                        </div>
                        <h4>Комиссия</h4>
                        Возьмем заботы по продаже вашего автомобиля на себя. </a>
                </div>
            </div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>