<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Автосалон Долкар");
?><!-- 1. Link to jQuery (1.8 or later), --> <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script> <!-- 33 KB --> <!-- fotorama.css & fotorama.js. --> <!-- 3 KB --> <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
<div class="carblock">
	<div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-ratio="4/3" data-loop="true">
 <a href="/upload/maxposter/267/353219/big/8025a1c7ea4e631087e402d656a74cdf.jpg" data-full="/upload/maxposter/267/353219/original/8025a1c7ea4e631087e402d656a74cdf.jpg"><img width="85" src="/upload/maxposter/267/353219/small/8025a1c7ea4e631087e402d656a74cdf.jpg" height="64"></a> <a href="/upload/maxposter/267/353219/big/52aa985f213c35a758a8b7baa0b7f865.jpg" data-full="/upload/maxposter/267/353219/original/52aa985f213c35a758a8b7baa0b7f865.jpg"><img width="85" src="/upload/maxposter/267/353219/small/52aa985f213c35a758a8b7baa0b7f865.jpg" height="64"></a> <a href="/upload/maxposter/267/353219/big/dc4c6d6256cc094b2545a1e87f861a07.jpg" data-full="/upload/maxposter/267/353219/original/dc4c6d6256cc094b2545a1e87f861a07.jpg"><img width="85" src="/upload/maxposter/267/353219/small/dc4c6d6256cc094b2545a1e87f861a07.jpg" height="64"></a> <a href="/upload/maxposter/267/353219/big/5b09da73dc3cb379251a8a4492125ce0.jpg" data-full="/upload/maxposter/267/353219/original/5b09da73dc3cb379251a8a4492125ce0.jpg"><img width="85" src="/upload/maxposter/267/353219/small/5b09da73dc3cb379251a8a4492125ce0.jpg" height="64"></a> <a href="/upload/maxposter/267/353219/big/9452d30f3e1dd3062962e348480335b7.jpg" data-full="/upload/maxposter/267/353219/original/9452d30f3e1dd3062962e348480335b7.jpg"><img width="85" src="/upload/maxposter/267/353219/small/9452d30f3e1dd3062962e348480335b7.jpg" height="64"></a> <a href="/upload/maxposter/267/353219/big/3043fc892f88455dd6bc2fd3fcdb6cfe.jpg" data-full="/upload/maxposter/267/353219/original/3043fc892f88455dd6bc2fd3fcdb6cfe.jpg"><img width="85" src="/upload/maxposter/267/353219/small/3043fc892f88455dd6bc2fd3fcdb6cfe.jpg" height="64"></a> <a href="/upload/maxposter/267/353219/big/2bf898857e499806c85104fe23063054.jpg" data-full="/upload/maxposter/267/353219/original/2bf898857e499806c85104fe23063054.jpg"><img width="85" src="/upload/maxposter/267/353219/small/2bf898857e499806c85104fe23063054.jpg" height="64"></a> <a href="/upload/maxposter/267/353219/big/fe3c149ecbb347e8d05b3eb987e9290a.jpg" data-full="/upload/maxposter/267/353219/original/fe3c149ecbb347e8d05b3eb987e9290a.jpg"><img width="85" src="/upload/maxposter/267/353219/small/fe3c149ecbb347e8d05b3eb987e9290a.jpg" height="64"></a>
	</div>
	<div class="element_params">
		<div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
			<h3>Renault Fluence I Рестайлинг</h3>
		</div>
		<div class="element_line">
			<p>
				 Год выпуска
			</p>
			 2014
		</div>
		<div class="element_line">
			<p>
				 Пробег
			</p>
			 2 335 км
		</div>
		<div class="element_line">
			<p>
				 Цена
			</p>
			 599&nbsp;000&nbsp;р.
		</div>
		 Кузовседан Тип двигателябензиновый Объем двигателя1598&nbsp;см³&nbsp;(114&nbsp;л.с.) Приводпередний Тип КППвариаторная Рульлевый Состояниеотличное Цветсеребристый металлик Кол-во хозяев по ПТСодин
	</div>
</div>
<div class="box1">
	<h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">Комплектация</h3>
</div>
<div class="row">
	<div class="grid_3">
 <span class="li">Антиблокировочная система (АБС)</span>
	</div>
	<div class="grid_3">
 <span class="li">Парктроник</span>
	</div>
	<div class="grid_3">
		 Подушки безопасности: передние
	</div>
	<div class="grid_3">
		 Охранная система
	</div>
	<div class="grid_3">
		 Центральный замок
	</div>
	<div class="grid_3">
		 Датчик дождя
	</div>
	<div class="grid_3">
		 Датчик света
	</div>
	<div class="grid_3">
		 Салон: ткань
	</div>
	<div class="grid_3">
		 Бортовой компьютер
	</div>
	<div class="grid_3">
		 Усилитель рулевого управления: гидро
	</div>
	<div class="grid_3">
		 Регулировка руля: в одной плоскости
	</div>
	<div class="grid_3">
		 Электрозеркала
	</div>
	<div class="grid_3">
		 Обогрев зеркал
	</div>
	<div class="grid_3">
		 Электростеклоподъемники: все
	</div>
	<div class="grid_3">
		 Обогрев сидений
	</div>
	<div class="grid_3">
		 Регулировка сиденья водителя: по высоте
	</div>
	<div class="grid_3">
		 Управление климатом: 2-зонный
	</div>
	<div class="grid_3">
		 Стереосистема: CD
	</div>
</div>
<div class="box1">
	<h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">Дополнительная информация</h3>
	<p>
		 Автомобиль представлен к продаже в Автосалоне ДИЛЕРСКОГО техцентра "ДОЛАВТО" с возможностью оценки технического состояния и проведения тест-драйва.
	</p>
	<p>
		 Вас ждёт светлый торговый зал (более 1000 кв.м.) с большим выбором автомобилей
	</p>
	<p>
		 Один владелец<br>
		 ПТС оригинал<br>
		 Дата регистрации 29.03.2014 г.<br>
		 Кузов в заводском окрасе<br>
		 ТО-0 сделано в марте 2015г.<br>
		 Состояние нового автомобиля
	</p>
	<p>
		 Двигатель 1598 куб. см. 114 л. с.<br>
		 CVT с возможностью ручного переключения<br>
		 4 SRS<br>
		 Климат-контроль 2-х зонный<br>
		 Подогрев передних сидений<br>
		 Дневные ходовые огни (диодные)<br>
		 Повторители поворотов в зеркалах заднего вида<br>
		 Противотуманные фары<br>
		 Корректор фар<br>
		 Центральный замок<br>
		 4 Эл. стеклоподъемника<br>
		 Эл. регулировка зеркал с подогревом<br>
		 Бортовой компьютер <br>
		 Штатная магнитола 6 колонок, CD, MP3, USB, AUX, Bluetooth<br>
		 Передние подстаканники<br>
		 Передний подлокотник с боксом<br>
		 Подсветка аксессуарных зеркал<br>
		 Регулировка водительского сиденья по высоте<br>
		 Спинка заднего сиденья складывается 40/60<br>
		 Isofix - крепление детских кресел
	</p>
	<p>
		 Резиновые коврики салона<br>
		 Резиновый коврик в салоне<br>
		 Передние брызговики<br>
		 Парктроники задние<br>
		 Сигнализация Scher-Khan
	</p>
	<p>
		 Возможен ВЫКУП или ЗАЧЕТ Вашего автомобиля.
	</p>
	<p>
		 Другие автомобили и схему проезда нашего автосалона Вы найдёте ниже в разделе контактной информации: строка АВТОЦЕНТР "ДОЛАВТО".
	</p>
</div>
<div class="grid_3">
</div>
 <!--========================================================
                          FOOTER
=========================================================-->
<div id="footer">
	<div class="width-wrapper width-wrapper__inset1 width-wrapper__inset2">
		<div class="wrapper4">
			<div class="container">
				<div class="row">
					<div class="grid_3">
						<div class="box2">
							<h5>Кто мы</h5>
							<ul class="list1">
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">Мы продаем автомобили в Долгопрудном, и, поверьте, делаем это хорошо!</li>
							</ul>
						</div>
					</div>
					<div class="grid_3">
						<div class="box2">
							<h5>Время работы</h5>
							<ul class="list1">
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">Понедельник&nbsp;09:00 - 21:00</li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">Вторник&nbsp;09:00 - 21:00</li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">Среда&nbsp;09:00 - 21:00</li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">Четверг&nbsp;09:00 - 21:00</li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">Пятница&nbsp;09:00 - 21:00</li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s">Суббота&nbsp;09:00 - 21:00</li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">Воскресенье&nbsp;09:00 - 21:00</li>
							</ul>
						</div>
					</div>
					<div class="grid_3">
						<div class="box2">
							<h5>Контакты</h5>
							<ul class="contacts-list">
								<li class="address wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><i class="fa fa-home"></i><a href="/contacts/">МКАД Север, 79-й км,<br>
								 г.Долгопрудный,<br>
								 Транспортный проезд, д.3<br>
								 (Напротив здания ГИБДД)</a></li>
								<li class="phone wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><i class="fa fa-phone"></i>+7-495-255-0025</li>
								<li class="email wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><i class="fa fa-envelope-o"></i><a href="mailto:sale@dolcar.ru">sale@dolcar.ru</a></li>
							</ul>
						</div>
					</div>
					<div class="grid_3">
						<div class="box2">
							<h5>Меню сайта</h5>
							<ul class="list1">
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="/avtomobili-s-probegom/" class="selected">Авто с пробегом</a></li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="/service/">Сервис</a></li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="/uslugi/strahovanie/">Страхование</a></li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="/uslugi/kreditovanie/">Кредитование</a></li>
								<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s"><a href="/contacts/">Контакты</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="wrapper5">
		<div class="width-wrapper width-wrapper__inset1 width-wrapper__inset3">
			<div class="container">
				<div class="row">
					<div class="grid_12">
						<div class="privacy-block wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
							 Copyright <a href="/">DolCar</a> © <span id="copyright-year">2015</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Yandex.Metrika counter 
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter14029747 = new Ya.Metrika({id:14029747,
                    webvisor:true,
                    clickmap:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/14029747" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<script src="js/script.js"></script>