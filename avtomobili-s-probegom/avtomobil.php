<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Автомобиль");
?><?$APPLICATION->IncludeComponent(
	"maxposter:api.vehicle",
	"",
	Array(
		"ADD_SECTIONS_CHAIN" => "Y",
		"COMPONENT_TEMPLATE" => ".default",
		"MAX_API_LOGIN" => "",
		"MAX_API_PASSWORD" => "",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_ADRESS" => "N",
		"SHOW_AVAILABILITY" => "N",
		"SHOW_COLOR" => "N",
		"SHOW_CONDITION" => "N",
		"SHOW_CONTACTS" => "N",
		"SHOW_CUSTOMS" => "N",
		"SHOW_DESCRIPTION" => "Y",
		"SHOW_INSPECTION" => "Y",
		"SHOW_MARK_MODEL" => "N",
		"SHOW_PTS" => "N",
		"URL_TEMPLATES_INDEX" => "/avtomobili-s-probegom/",
		"URL_TEMPLATES_VEHICLE" => "/avtomobili-s-probegom/vehicle.php?VEHICLE_ID=#VEHICLE_ID#",
		"VEHICLE_ID" => $_REQUEST["VEHICLE_ID"]
	)
);?><br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>