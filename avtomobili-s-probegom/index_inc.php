<div class="banner1">
	<h2 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"> <a href="#">Выбирай из лучшего</a> </h2>
	<p class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
		 Проверенные по всем параметрам автомобили. Гарантия технического состояния и юридической чистоты. Великолепные цены. Выбор из наличия, либо из нашей базы .
	</p>
</div>
<div class="do-it">
	<h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">Экспертиза Dolcar</h3><br>
	<ul class="st">
		<li> <a href="#"><i class="icon i-tech-report"></i> Экспертиза технического состояния</a></li>
		<li> <a href="#"><i class="icon i-legal"></i> Проверка юридической чистоты </a></li>
		<li> <a href="#"><i class="icon i-presale"></i> Предпродажная подготовка </a></li>
		<li> <a href="#"><i class="icon i-key-exchange"></i> Безопасное сопровождение сделок </a></li>
	</ul>
</div>
<div class="do-it">
	<h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">Отзывы клиентов</h3><br>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"otzyv",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"COMPONENT_TEMPLATE" => "otzyv",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(0=>"",1=>"",),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "2",
		"IBLOCK_TYPE" => "10",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "3",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Отзывы",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(0=>"",1=>"",),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC"
	)
);?>
<a href="/otzyvy">Все отзывы</a>

</div>
<!--div class="share">
						<h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">Поделиться</h3>
   <a href="http://twitter.com/?status=Expert Car Advice From the AA - AA New Zealand: http://www.aa.co.nz/cars/" class="share--twitter" target="_blank" onclick="_gaq.push(['_trackEvent', 'Share this page Widget', 'Social Share', 'Twitter | http://www.aa.co.nz/cars/'])"><i class="icon icon-twitter-circle"></i></a>
                <a href="http://www.facebook.com/sharer/sharer.php?u=http://www.aa.co.nz/cars/&t=Expert Car Advice From the AA - AA New Zealand" class="share--facebook" target="_blank" onclick="_gaq.push(['_trackEvent', 'Share this page Widget', 'Social Share', 'Facebook | http://www.aa.co.nz/cars/'])"><i class="icon icon-facebook-circle"></i></a>
                <a href="https://plus.google.com/share?url=http://www.aa.co.nz/cars/" class="share--google-plus" target="_blank" onclick="_gaq.push(['_trackEvent', 'Share this page Widget', 'Social Share', 'Google+ | http://www.aa.co.nz/cars/'])"><i class="icon icon-google-plus"></i></a>
</div>

<div class="post1">
						<h2 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">Наш обзор</h2>
 <img src="images/page1_img1.png" class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" alt=""> <time class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s" datetime="2014-01-01">Monday, June 24, 2013</time>
						<h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"> <a href="#">Opel Insignia</a> </h3>
						<p class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
							 Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.						</p>
					</div-->