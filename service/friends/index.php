<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Новый раздел");
?><p class="block">
 <span style="color: #f16522;"><b><u><span style="font-size: 16pt;">Сервис для друзей</span></u></b></span>
</p>
<p class="block">
	 Покупателям наших авто мы предлагаем дальнейшее сервисное обслуживание автомобилей. <br>
	 Мы хорошо знаем Ваш автомобиль - ведь мы уже осматривали его перед продажей, и не станем навязывать ремонт несуществующих недостатков.&nbsp;
</p>
<p class="block">
	 Обслуживание проводится на технической базе автосервиса ДОЛАВТО. &nbsp;
</p>
<p class="block">
	 Мы заинтересованы в том, чтобы Вы проходили все необходимое от покупки автомобиля с пробегом до заказа запчастей и ремонта в одном месте - и предложим честную цену на ремонт и обслуживание авто.&nbsp;
</p>
<p class="block">
</p>
<ul>
	<li>
	<p class="block">
		 Ремонт подвески автомобиля ремонт КПП и трансмиссии
	</p>
 </li>
	<li>
	<p class="block">
		 Ремонт двигателя автомобиля&nbsp;
	</p>
 </li>
	<li>
	<p class="block">
		 ремонт автомобильной электроники&nbsp;
	</p>
 </li>
	<li>
	<p class="block">
		 ремонт двигателя автомобиля&nbsp;
	</p>
 </li>
	<li>
	<p class="block">
		 Ремонт и заправка кондиционеров&nbsp;
	</p>
 </li>
</ul>
<p class="block">
</p>
<p class="block">
	 Помимо этого Вы всегда можете заказать у нас установку дополнительного оборудования на Ваш автомобиль&nbsp;
</p>
<p class="block">
</p>
<ul>
	<li>
	<p class="block">
		 Установка сигнализаций&nbsp;
	</p>
 </li>
	<li>
	<p class="block">
		 Установка мультимедийных систем&nbsp;
	</p>
 </li>
	<li>
	<p class="block">
		 Перетяжку салона
	</p>
 </li>
</ul>
<p class="block">
	 &nbsp;<img width="614" alt="avtoservis_new24.jpg" src="/upload/medialibrary/796/7966d5c6971567e5e3c44ece7bc98bf4.jpg" height="384" title="avtoservis_new24.jpg">
</p>
<p class="block">
	 и многие другие услуги напишите нам, и мы приятно удивим Вас ценой!
</p>
<p class="block">
 <br>
 <a href="#form_question" class="question btn-std">Задать вопрос</a> <a href="#service_order" class="question btn-std">Заказать услугу</a>
</p>
<div id="form_question" style="display: none">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"form_question",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => "form_question",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"LIST_URL" => "result_list.php",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
		"WEB_FORM_ID" => "4"
	)
);?>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>