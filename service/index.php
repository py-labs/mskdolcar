<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Сервис");
?>
    <div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
        <h2>Мы заботимся об автомобилях.</h2>
    </div>
    <div class="row">
        <div class="col-sm-9">
            <ul>
                <li class="li">Ремонт электронных блоков управления любых автомобилей<br>
                </li>
                <li class="li">Восстановление запуска автомобиля, ремонт штатных иммобилайзеров<br>
                </li>
                <li class="li">Ремонт панелей приборов автомобилей, мотоциклов, квадроциклов, снегоходов и любой другой техники в которой есть электронная приборная панель<br>
                </li>
                <li class="li">Ремонт и восстановления&nbsp;систем SRS, AIRBAG<br>
                </li>
                <li class="li">Программное отключение сажевых фильтров и катализаторов<br>
                </li>
                <li class="li">Программное отключение ошибок<br>
                </li>
                <li class="li">Чип тюнинг автомобилей<br>
                </li>
                <li class="li">Восстановление блоков управления после неудачного ремонта<br>
                </li>
                <li class="li">Ремонт диагностического оборудования<br>
                </li>
                <li class="li">Установка и настройка программного обеспечения для диагностики&nbsp;</li>
            </ul>
            <div class="box1">
                <h2>Ремонт автомобильной электроники</h2>
                <br>
            </div>
            <div style="padding-bottom: 20px">
                <h2>
                    Дополнительное оборудование </h2>
                <br>
                &nbsp; &nbsp; &nbsp;Коллектив технически подкованных специалистов DolCar поможет Вам с выбором и установкой аудио- и видео- компонентов, противоугонных систем и любых индивидуальных решений для удобства и безопасной поездки в автомобиле. Нами охвачены различные марки и классы авто-мото техники по установке дополнительного и штатного оборудования.<br>
                &nbsp; &nbsp; &nbsp;Для каждого водителя машина- это не просто игрушка на колесах и мы это понимаем, поэтому отношение к каждому автомобилю у нас трепетное, буквально вкладываем душу и подбираем наивыгоднейшее решение для поставленной задачи.<br>
            </div>
        </div>
        <div class="col-sm-3 text-center">
            <img width="315" alt="elektro.jpg" src="/upload/medialibrary/8a2/8a26b05de179d1aa47bf85916defbadd.jpg" title="elektro.jpg"><br>
            <img width="350" alt="wpac5ad273_1a.jpg" src="/upload/medialibrary/fb3/fb3beade9d2969829cfbc238f6b2ebb3.jpg" title="wpac5ad273_1a.jpg"><br>
        </div>
    </div>
<br />
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>