<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Новый раздел");
?>
    <div class="block">
        <h2><span style="font-size: 15pt; color: #f16522;"><b><u>Ремонт автомобильной электроники в Долгопрудном.</u></b></span></h2>
        <br>
        <div class="row">
            <div class="col-sm-9">
                <ul>
                    <li>Ремонт электронных блоков управления любых автомобилей</li>
                    <li>Восстановление запуска автомобиля, ремонт штатных иммобилайзеров</li>
                    <li>Ремонт панелей приборов автомобилей, мотоциклов, квадроциклов, снегоходов и любой другой техники в которой есть электронная приборная панель</li>
                    <li>Ремонт и восстановления&nbsp;систем SRS, AIRBAG</li>
                    <li>Программное отключение сажевых фильтров и катализаторов</li>
                    <li>Программное отключение ошибок</li>
                    <li>Чип тюнинг автомобилей</li>
                    <li>Восстановление блоков управления после неудачного ремонта</li>
                    <li>Ремонт диагностического оборудования</li>
                    <li>Установка и настройка программного обеспечения для диагностики&nbsp;</li>
                </ul>
                <br>
                <a href="#form_question" class="question btn-std">Задать вопрос</a> <a href="#service_order" class="question btn-std">Заказать услугу</a><br><br>
            </div>
            <div class="col-sm-3 text-center">
                <img width="315" alt="elektro.jpg" src="/upload/medialibrary/8a2/8a26b05de179d1aa47bf85916defbadd.jpg" title="elektro.jpg"><br>
                <img width="350" alt="wpac5ad273_1a.jpg" src="/upload/medialibrary/fb3/fb3beade9d2969829cfbc238f6b2ebb3.jpg" title="wpac5ad273_1a.jpg"><br>
            </div>
        </div>

    </div>
    <div id="form_question" style="display: none">
        <? $APPLICATION->IncludeComponent(
            "bitrix:form.result.new",
            "form_question",
            Array(
                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "A",
                "CHAIN_ITEM_LINK" => "",
                "CHAIN_ITEM_TEXT" => "",
                "COMPONENT_TEMPLATE" => "form_question",
                "EDIT_URL" => "result_edit.php",
                "IGNORE_CUSTOM_TEMPLATE" => "Y",
                "LIST_URL" => "result_list.php",
                "SEF_MODE" => "N",
                "SUCCESS_URL" => "",
                "USE_EXTENDED_ERRORS" => "Y",
                "VARIABLE_ALIASES" => array("WEB_FORM_ID" => "WEB_FORM_ID", "RESULT_ID" => "RESULT_ID",),
                "WEB_FORM_ID" => "4"
            )
        ); ?>
    </div>
    <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>