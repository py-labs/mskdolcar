<?$APPLICATION->IncludeComponent(
	"kreattika:forms.feedback", 
	"feedback", 
	array(
		"COMPONENT_TEMPLATE" => "feedback",
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"USE_FIELD_NAME" => "Y",
		"CHECK_FIELD_NAME" => "Y",
		"FIELD_NAME_TITLE" => "Имя:",
		"USE_FIELD_PHONE" => "Y",
		"CHECK_FIELD_PHONE" => "Y",
		"FIELD_PHONE_TITLE" => "Телефон:",
		"USE_FIELD_EMAIL" => "Y",
		"CHECK_FIELD_EMAIL" => "N",
		"FIELD_EMAIL_TITLE" => "Email:",
		"USE_FIELD_TEXT" => "Y",
		"CHECK_FIELD_TEXT" => "N",
		"FIELD_TEXT_TITLE" => "Текст:",
		"USE_CAPTCHA" => "Y",
		"SUBMIT_TITLE" => "Отправить",
		"OK_TEXT" => "Ваше сообщение принято. Наши менеджеры свяжутся с Вами в самое ближайшее время. Удачного дня.",
		"EMAIL_TO" => "sales@dolcar.ru",
		"EVENT_MESSAGE_ID" => array(
			0 => "26",
		)
	),
	false
);?>