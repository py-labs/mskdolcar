<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Автосалон Долкар I Долгопудный автомобили с пробегом");
$APPLICATION->SetTitle("Автосалон Долкар I Долгопудный автомобили с пробегом");
?><div class="slider-wrapper">
    <div id="camera_wrap">
        <?$APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "slider",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => "slider",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(0=>"PREVIEW_TEXT",1=>"",),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "3",
                "IBLOCK_TYPE" => "content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "20",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(0=>"URL",1=>"",),
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC"
            )
        );?>
    </div>
    <div class="clearfix">
    </div>
</div>
    <div class="after-banner">
        <div class="after-item">
            <a href="about/">О нас</a>
        </div>
        <div class="after-item">
            <a href="articles/">Статьи</a>
        </div>
        <div class="after-item">
            <a href="otzyvy/">Отзывы</a>
        </div>
        <div class="after-item">
            <a href="guarantees/">Гарантии</a>
        </div>
    </div>
    <div class="wrapper2">
        <?$APPLICATION->IncludeComponent(
            "maxposter:api.special_offers",
            "spec",
            Array(
                "ADD_SECTIONS_CHAIN" => "Y",
                "COMPONENT_TEMPLATE" => "spec",
                "MAX_API_LOGIN" => "267",
                "MAX_API_PASSWORD" => "dolauto267",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "URL_TEMPLATES_INDEX" => "/avtomobili-s-probegom/",
                "URL_TEMPLATES_VEHICLE" => "/avtomobili-s-probegom/vehicle.php?VEHICLE_ID=#VEHICLE_ID#"
            )
        );?>
        <div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
            <h2>Последние поступления</h2>
        </div>
        <?$APPLICATION->IncludeComponent(
            "maxposter:api.list",
            "latest",
            Array(
                "ADD_SECTIONS_CHAIN" => "Y",
                "COMPONENT_TEMPLATE" => "latest",
                "MAX_API_LOGIN" => "267",
                "MAX_API_PASSWORD" => "dolauto267",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "URL_TEMPLATES_INDEX" => "/avtomobili-s-probegom/",
                "URL_TEMPLATES_VEHICLE" => "/avtomobili-s-probegom/vehicle.php?VEHICLE_ID=#VEHICLE_ID#"
            )
        );?>
    </div>
    &nbsp;<br>

    <script src="https://msto.me/widget.js?user=dolcar"></script>

    <!-- BEGIN JIVOSITE CODE {literal} -->
    <script type='text/javascript'>
        (function(){ var widget_id = 'TwYYxwxChN';var d=document;var w=window;function l(){
            var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
    <!-- {/literal} END JIVOSITE CODE -->
    <script async src="https://widget.calltovisit.com/widget.js?config=8cb9b3f676e947fa75f9063c54d477cb"></script>
    <script>
        window.ctvsData = window.ctvsData || [];
        function ctvs(){ctvsData.push(arguments)};
        ctvs('config', '8cb9b3f676e947fa75f9063c54d477cb');
    </script><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>