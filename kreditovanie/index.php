<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Кредитование");
?><blockquote>
	<div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
		<h2>Кредитование автомобилей с пробегом</h2>
	</div>
	<div class="row">
	</div>
	<div class="row">
	</div>
	<div class="row">
	</div>
</blockquote>
<blockquote>
	<p class="row" style="text-align: center;">
 <span style="font-size: 16pt;"><b>Преимущества кредитования в ДОЛКАР:</b></span>
	</p>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
		<table border="1" cellspacing="0" cellpadding="0" align="center">
		<tbody>
		<tr>
			<td style="text-align: center;">
				<p>
 <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u>Онлайн заявка. &nbsp;</u></span></b></span>
				</p>
				<p style="text-align: center;">
					 Отправьте заявку онлайн без необходимости &nbsp;посещения офиса и траты своего времени. &nbsp;
				</p>
				<p style="text-align: center;">
				</p>
				<p style="text-align: center;">
 <img width="77" alt="6.png" src="/upload/medialibrary/1c1/1c18a6824838c7ea4709f3aecbe45e3a.png" height="67" title="6.png">
				</p>
				<p>
 <br>
				</p>
			</td>
			<td style="text-align: center;">
				<p style="text-align: center;">
 <b><span style="font-size: 16pt; color: #f16522;"><u>Сумма от 250 тысяч и до 3 млн. рублей</u></span></b>
				</p>
				<p>
					 Суммы вполне хватает для покупки нового или поддержанного автомобиля от признанных производителей.
				</p>
				<p>
 <img width="77" alt="3.png" src="/upload/medialibrary/b72/b726b2adced29640f726e24571d88a04.png" height="67" title="3.png"><br>
				</p>
				<p>
 <br>
				</p>
			</td>
			<td style="text-align: center;">
				<p style="text-align: center;">
 <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u>Индивидуальный подбор программы кредитования.</u></span></b></span>
				</p>
				<p>
					 Кредитный специалист поможем выбрать кредитную программу, исходя из особенностей вашего положения.
				</p>
				<p>
 <img width="77" alt="9.png" src="/upload/medialibrary/87e/87e1c16d579da70f4160ebd93bf9c8d4.png" height="67" title="9.png"><br>
				</p>
				<p>
 <br>
				</p>
				<p>
 <br>
				</p>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;">
				<p>
				</p>
				<p style="text-align: center;">
 <b><span style="font-size: 16pt;"><span style="color: #f16522;"><u>Кредит по 2 документам.</u></span></span></b>
				</p>
				<p style="text-align: center;">
				</p>
				<p style="text-align: left;">
				</p>
				<p style="text-align: center;">
					 Для рассмотрения Вашей заявки будет достаточно предоставить паспорт и водительское удостоверение.
				</p>
				<p style="text-align: center;">
 <img width="77" alt="8.png" src="/upload/medialibrary/40c/40cdf7a54d385d6f872036c3917e68e6.png" height="67" title="8.png">
				</p>
				<p style="text-align: left;">
				</p>
				<p>
				</p>
				<p>
 <br>
				</p>
			</td>
			<td style="text-align: center;">
				<p style="text-align: center;">
 <b><span style="font-size: 16pt; color: #f16522;"><u>Без КАСКО и страхования жизни.&nbsp;</u></span></b>
				</p>
				<p>
					 Не навязываем заемщикам дополнительных требований и не обременяем платой по КАСКО.
				</p>
				<p>
 <img width="77" alt="1.png" src="/upload/medialibrary/d5e/d5e41c7610e448fbfdc3f571c7f7f958.png" height="67" title="1.png"><br>
				</p>
				<p>
 <br>
				</p>
			</td>
			<td style="text-align: center;">
				<p style="text-align: center;">
 <b><span style="font-size: 16pt;"><span style="color: #f16522;"><u>Срок кредитования от 1 года и до 5 лет.</u></span></span></b>
				</p>
				<p>
					 Длительный период кредитования снижает процентную ставку и понижает ежемесячные платежи
				</p>
				<p>
 <img width="77" alt="7.png" src="/upload/medialibrary/29f/29fac5ea4e9d29ff9146da4dcf2e5c36.png" height="67" title="7.png"><br>
				</p>
				<p>
 <br>
				</p>
			</td>
		</tr>
		<tr>
			<td style="text-align: center;">
				<p>
				</p>
				<p style="text-align: center;">
 <b><span style="font-size: 16pt; color: #f16522;"><u>Без справок.</u></span></b>
				</p>
				<p style="text-align: center;">
				</p>
				<p style="text-align: center;">
					 Нет необходимости предоставлять справки с места работы и других организаций.
				</p>
				<p style="text-align: center;">
				</p>
				<p style="text-align: center;">
				</p>
				<p style="text-align: center;">
				</p>
				<p style="text-align: center;">
 <img width="77" alt="2.png" src="/upload/medialibrary/545/545208cd681dd3747b6a80316a9a4b4e.png" height="67" title="2.png">
				</p>
			</td>
			<td style="text-align: center;">
				<p>
				</p>
				<p style="text-align: center;">
 <b><span style="color: #f16522;"><u><span style="font-size: 16pt;">Проведение сделок С2С ( продавец и покупатель частные лица)&nbsp; </span></u></span></b>&nbsp; &nbsp;&nbsp;
				</p>
				<p style="text-align: center;">
				</p>
				<p style="text-align: center;">
					 Полное сопровождение Вашей сделки купли-продажа автомобиля. Подготовка и оформление всех необходимых документов и проверок.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				</p>
				<p>
					 &nbsp; &nbsp;<img width="77" alt="5.png" src="/upload/medialibrary/937/9375f6142ded1970ac6f36459f9c8b43.png" height="67" title="5.png">
				</p>
 <b><u> </u></b>
			</td>
			<td style="text-align: center;">
 <b><u> </u></b>
				<p style="text-align: center;">
 <b> <span style="font-size: 16pt; color: #f16522;"><span style="color: #f16522;"><u>Проверка юридической чистоты автомобиля.</u></span></span></b>
				</p>
				<p>
					 При покупке Вами поддержанного автомобиля проверяем его юридическую чистоту и отсутствие «криминального прошлого».
				</p>
				<p>
 <img width="77" alt="4.png" src="/upload/medialibrary/fcc/fccc178c835ac1bd9552477d89b119db.png" height="67" title="4.png"><br>
				</p>
			</td>
		</tr>
		</tbody>
		</table>
 <br>
 <br>
 <br>
	</div>
	<div class="row">
 <img width="868" alt="avtocredit.png" src="/upload/medialibrary/332/332fba8176580541abe29e361196e482.png" height="395" title="Автокредиты"><br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
		<blockquote>
			<blockquote>
				<h2>Кредитование автомобилей с пробегом</h2>
				 СРОК КРЕДИТОВАНИЯ ОТ 1 ГОДА ДО 5 ЛЕТ ВКЛЮЧИТЕЛЬНО<br>
				 НАЛИЧИЕ КРЕДИТНЫХ ПРОГРАММ БЕЗ КАСКО И СТРАХОВАНИЯ ЖИЗНИ<br>
				 ОПТИМАЛЬНЫЕ ПРОГРАММЫ КРЕДИТОВАНИЯ ПОД ЛЮБОГО КЛИЕНТА<br>
				 ПРОВЕДЕНИЕ СДЕЛОК КУПЛИ-ПРОДАЖИ C2C (ПРОДАВЕЦ И ПОКУПАТЕЛЬ ЧАСТНЫЕ ЛИЦА)<br>
				 РАБОТАЕМ ДО ПОСЛЕДНЕГО КЛИЕНТА<br>
			</blockquote>
			<p>
 <br>
			</p>
			<p>
				 Для решившихся приобрести личный автомобиль в большинстве своём оптимальном вариантом является оформление автокредита.&nbsp;
			</p>
		</blockquote>
		<blockquote>
			<p>
				 В этом случае не придётся долго и мучительно копить нужную сумму. Многие организации предлагают оформить автокредит на выгодных условиях с совсем небольшими переплатами.
			</p>
		</blockquote>
		<blockquote>
			<p>
 <span style="font-size: 16pt;"><b>Как выбирать программу автокредитования?</b></span>
			</p>
		</blockquote>
		<blockquote>
			<p>
 <br>
			</p>
		</blockquote>
		<blockquote>
			 Первым делом необходимо обратить внимание на процентную ставку. Однако необходимо смотреть и на дополнительные платежи, комиссии и иные затраты, которые предусмотрены условиями кредита. Также необходимо выяснить способ расчета, будет ли это дифференцированный или аннуитетный вариант. В первом случае сумма взноса понижается ежемесячно, так как уменьшается основной долг. Во втором случае предусмотрено внесение одинаковых денежных сумм на протяжении всего срока кредитования. Кредитные специалисты рекомендуют выбирать кредит в той валюте, в которой заёмщик получает зарплату.&nbsp;<br>
		</blockquote>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
	<div class="row">
 <br>
	</div>
</blockquote>
 &nbsp;<br>
<div class="row">
</div>
<div class="row">
 <b><u> </u></b>
</div>
 <br><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>