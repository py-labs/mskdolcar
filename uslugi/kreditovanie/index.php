<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("TITLE", "Купить автомобиль в кредит в Долгопрудном I ДОЛКАР");
$APPLICATION->SetPageProperty("description", "Кредитование в автосалоне Долкар. Кредит для покупки автомобиля с пробегом.");
$APPLICATION->SetTitle("Купить автомобиль с пробегомм в кредит  I ДОЛКАР Долгопрудный");
?>

    <div class="block text-right">
        <h3>Авто с пробегом в кредит.</h3>
        Подберем кредитную программу.<br>
        <br>
        <a href="#form_credit" class="credit btn-std">Оставить заявку на кредит</a>
    </div>
    <blockquote>
        <div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
            <h2 style="text-align: center;"><span style="font-size: 16pt;"><b><u>Кредитование автомобилей с пробегом в Долгопрудном</u></b></span></h2>
            <h2 style="text-align: center;"><b style="font-size: 16pt;"><span style="color: #000000;"><u>Преимущества кредитования в ДОЛКАР:</u></span></b></h2>
        </div>
    </blockquote>
    <blockquote class="text-center">
        <div class="row">
            <div class="col-md-4">
                <p>
                    <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u>Онлайн заявка.</u></span></b></span>
                </p>
                <p style="text-align: center;">
                    Отправьте заявку онлайн без необходимости посещения офиса и траты своего времени.
                </p>
                <p style="text-align: center;">
                    <img width="77" alt="6.png" src="/upload/medialibrary/1c1/1c18a6824838c7ea4709f3aecbe45e3a.png" height="67" title="6.png">
                </p>
            </div>
            <div class="col-md-4">
                <p style="text-align: center;">
                    <b><span style="font-size: 16pt; color: #f16522;"><u>Сумма от 250 тысяч и до 3 млн. рублей</u></span></b>
                </p>
                <p style="text-align: center;">
                    Суммы вполне хватает для покупки нового или поддержанного автомобиля от признанных производителей.
                </p>
                <p style="text-align: center;">
                    <img width="77" alt="3.png" src="/upload/medialibrary/b72/b726b2adced29640f726e24571d88a04.png" height="67" title="3.png">
                </p>
            </div>
            <div class="col-md-4">
                <p style="text-align: center;">
                    <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u>Индивидуальный подбор программы кредитования.</u></span></b></span>
                </p>
                <p>
                    Кредитный специалист поможем выбрать кредитную программу, исходя из особенностей вашего положения.
                </p>
                <p>
                    <img width="77" alt="9.png" src="/upload/medialibrary/87e/87e1c16d579da70f4160ebd93bf9c8d4.png" height="67" title="9.png">
                </p>
            </div>
        </div>
        <br /><br />

        <div class="row">
            <div class="col-md-4">
                <p style="text-align: center;">
                    <b><span style="font-size: 16pt;"><span style="color: #f16522;"><u>Кредит по двум&nbsp;документам.</u></span></span></b><br>
                </p>
                <p style="text-align: center;">
                </p>
                <p style="text-align: left;">
                </p>
                <p style="text-align: center;">
                    Для рассмотрения Вашей заявки будет достаточно предоставить паспорт и водительское удостоверение.
                </p>
                <p style="text-align: center;">
                    <img width="87" alt="8.png" src="/upload/medialibrary/40c/40cdf7a54d385d6f872036c3917e68e6.png" height="75" title="8.png">
                </p>
            </div>
            <div class="col-md-4">
                <p style="text-align: center;">
                    <b><span style="font-size: 16pt; color: #f16522;"><u>Без КАСКО и страхования жизни.&nbsp;</u></span></b><br>
                </p>
                <p>
                    Не навязываем заемщикам дополнительных требований и не обременяем платой по КАСКО.
                </p>
                <p>
                    <img width="87" alt="1.png" src="/upload/medialibrary/d5e/d5e41c7610e448fbfdc3f571c7f7f958.png" height="75" title="1.png"><br>
                </p>
            </div>
            <div class="col-md-4">
                <p style="text-align: center;">
                    <b><span style="font-size: 16pt;"><span style="color: #f16522;"><u>Срок кредитования от 1 года и до 5 лет.</u></span></span></b>
                </p>
                <p>
                    Длительный период кредитования снижает процентную ставку и понижает ежемесячные платежи
                </p>
                <p>
                    <img width="77" alt="7.png" src="/upload/medialibrary/29f/29fac5ea4e9d29ff9146da4dcf2e5c36.png" height="67" title="7.png"><br>
                </p>
            </div>
        </div>
        <br /><br />
        <div class="row">
            <div class="col-md-4">
                <p style="text-align: center;">
                    <b><span style="font-size: 16pt; color: #f16522;"><u>Без справок.</u></span></b>
                </p>
                <p style="text-align: center;">
                </p>
                <p style="text-align: center;">
                    Нет необходимости предоставлять справки с места работы и других организаций. Кредитование возможно без дополнительных документов
                </p>
                <p style="text-align: center;">
                    <img width="77" alt="2.png" src="/upload/medialibrary/545/545208cd681dd3747b6a80316a9a4b4e.png" height="67" title="2.png">
                </p>
            </div>
            <div class="col-md-4">
                <p style="text-align: center;">
                    <b><span style="color: #f16522;"><u><span style="font-size: 16pt;">Проведение сделок С2С (продавец и покупатель частные лица)&nbsp; </span></u></span></b>&nbsp; &nbsp;&nbsp;<br>
                </p>
                <p style="text-align: center;">
                </p>
                <p style="text-align: center;">
                    Полное сопровождение Вашей сделки купли-продажа автомобиля. Подготовка и оформление всех необходимых документов и проверок.&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                </p>
                <p>
                    &nbsp; &nbsp;<img width="77" alt="5.png" src="/upload/medialibrary/937/9375f6142ded1970ac6f36459f9c8b43.png" height="67" title="5.png">
                </p>
            </div>
            <div class="col-md-4">
                <p style="text-align: center;">
                    <b><span style="font-size: 16pt; color: #f16522;"><u>Проверка юридической чистоты автомобиля.</u></span></b><br>
                </p>
                <p>
                    При покупке Вами поддержанного автомобиля проверяем его юридическую чистоту и отсутствие «криминального прошлого».
                </p>
                <p>
                    <img width="77" alt="4.png" src="/upload/medialibrary/fcc/fccc178c835ac1bd9552477d89b119db.png" height="67" title="4.png"><br>
                </p>
            </div>
        </div>
    </blockquote>
    <p style="text-align: right;">
        <br>
    </p>
    <blockquote>
        <div class="row">
            <blockquote>
                <p>
                    Для решивших&nbsp;приобрести личный автомобиль в большинстве своём оптимальном вариантом является оформление автокредита.&nbsp;
                </p>
            </blockquote>
            <blockquote>
                <p>
                    В этом случае не придётся долго и мучительно копить нужную сумму. Многие банки&nbsp;предлагают оформить автокредит на выгодных условиях с совсем небольшими переплатами.
                </p>
            </blockquote>
            <blockquote>
                <p>
                    <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u>Как выбирать программу автокредитования?</u></span></b></span>
                </p>
            </blockquote>
            <blockquote>
                Первым делом необходимо обратить внимание на процентную ставку. Однако необходимо смотреть и на дополнительные платежи, комиссии и иные затраты, которые предусмотрены условиями кредита. Также необходимо выяснить способ расчета, будет ли это дифференцированный или иной&nbsp;вариант. В первом случае сумма взноса понижается ежемесячно, так как уменьшается основной долг. Во втором случае предусмотрено внесение одинаковых денежных сумм на протяжении всего срока кредитования. Кредитные специалисты рекомендуют выбирать кредит в той валюте, в которой заёмщик получает зарплату.&nbsp;<br>
            </blockquote>
        </div>
        <div class="row">
            <br>
        </div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:news.list",
            "credit_auto_list",
            Array(
                "ACTIVE_DATE_FORMAT" => "d.m.Y",
                "ADD_SECTIONS_CHAIN" => "Y",
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "N",
                "AJAX_OPTION_STYLE" => "Y",
                "CACHE_FILTER" => "N",
                "CACHE_GROUPS" => "Y",
                "CACHE_TIME" => "36000000",
                "CACHE_TYPE" => "A",
                "CHECK_DATES" => "Y",
                "COMPONENT_TEMPLATE" => "credit_auto_list",
                "DETAIL_URL" => "",
                "DISPLAY_BOTTOM_PAGER" => "Y",
                "DISPLAY_DATE" => "Y",
                "DISPLAY_NAME" => "Y",
                "DISPLAY_PICTURE" => "Y",
                "DISPLAY_PREVIEW_TEXT" => "Y",
                "DISPLAY_TOP_PAGER" => "N",
                "FIELD_CODE" => array(0 => "", 1 => "",),
                "FILTER_NAME" => "",
                "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                "IBLOCK_ID" => "4",
                "IBLOCK_TYPE" => "content",
                "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
                "INCLUDE_SUBSECTIONS" => "Y",
                "MESSAGE_404" => "",
                "NEWS_COUNT" => "4",
                "PAGER_BASE_LINK_ENABLE" => "N",
                "PAGER_DESC_NUMBERING" => "N",
                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                "PAGER_SHOW_ALL" => "N",
                "PAGER_SHOW_ALWAYS" => "N",
                "PAGER_TEMPLATE" => ".default",
                "PAGER_TITLE" => "Новости",
                "PARENT_SECTION" => "",
                "PARENT_SECTION_CODE" => "",
                "PREVIEW_TRUNCATE_LEN" => "",
                "PROPERTY_CODE" => array(0 => "ENGINE", 1 => "MILEAGE", 2 => "PRICE", 3 => "",),
                "SET_BROWSER_TITLE" => "Y",
                "SET_LAST_MODIFIED" => "N",
                "SET_META_DESCRIPTION" => "Y",
                "SET_META_KEYWORDS" => "Y",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y",
                "SHOW_404" => "N",
                "SORT_BY1" => "ACTIVE_FROM",
                "SORT_BY2" => "SORT",
                "SORT_ORDER1" => "DESC",
                "SORT_ORDER2" => "ASC"
            )
        ); ?><br>
    </blockquote>
    <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>