<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetPageProperty("tags", "avtostrahovanie osago kasko strahovka auto");
$APPLICATION->SetPageProperty("TITLE", "Автострахование ОСАГО КАСКО в Долгопрудном I ДОЛКАР");
$APPLICATION->SetTitle("Cтрахование автомобиля в Долгопрудном");
?>
    <div class="text-right">
        <h3>Новый вид страховки.</h3>
        За небольшую сумму у нас можно приобрести МиниКАСКО.<br>
        <br>
        <div class="block">
            <a href="#form_strahovanie" class="credit btn-std">Заявка на страховку</a>
        </div>
    </div>
    <br/>
    <b><span style="color: #f16522;"><u><span style="font-size: 16pt;">
<p style="text-align: center;">
	 Страхование ОСАГО и КАСКО в Долгопрудном
</p>
 </span></u></span></b>
    Предсказуемых ситуаций на дороге не бывает, и даже самые опытные водители с многолетним стажем вождения не могут дать 100% гарантию своей безопасности. Этому может послужить множество факторов, начиная от попадания в ДТП и заканчивая плохими погодными условиями. А заключив договор автострахования в компании «Долкар» Вы снизите до минимума риски связанные с владением автомобилем.
    <br/><br/>
    <p align="center">
        <b style="font-size: 16pt;"><u><span style="color: #f16522;">ОСАГО</span></u></b><br>
    </p>
    <p style="text-align: center;">
        <span style="font-size: 16pt;"><b><u>Обязательное страхование автогражданской ответственности.</u></b></span>
    </p>
    <p style="text-align: center;">
 <span style="font-size: 16pt;"><b><u><br>
 </u></b></span>
    </p>
    <p align="center">
    </p>
    <div class="row text-center">
        <div class="col-md-4">
            <p style="text-align: center;">
                <b><span style="color: #f16522;"><u><span style="font-size: 14pt;">Стоимость ниже, чем в страховой компании. Минимальный тариф от 3000 рублей.</span></u></span></b>
            </p>
            <p>
                В отличии от страховых компаний, мы не заинтересованы, чтобы Вы купили полис в у конкретного страховщика, поэтому покажем и расскажем, где дешевле.
            </p>
            <p style="text-align: center;">
                <img width="77" alt="3.png" src="/upload/medialibrary/b72/b726b2adced29640f726e24571d88a04.png" height="67" title="3.png"><br>
            </p>
        </div>
        <div class="col-md-4">
            <p style="text-align: center;">
                <u><span style="font-size: 16pt;"> </span></u><span style="color: #f16522;"><b><u><span style="font-size: 14pt;">Ведём деятельности без наценок. Нам платят страховые компании.</span></u></b></span>
            </p>
            <p>
                Мы получаем вознаграждение непосредственно от страховых компаний в качестве страхового брокера.
            </p>
            <p style="text-align: center;">
                <img width="77" alt="12.png" src="/upload/medialibrary/b15/b15e6171ce9bda1efc56503fa9982c6a.png" height="67" title="12.png"><br>
            </p>
        </div>
        <div class="col-md-4">
            <p style="text-align: center;">
                <b><span style="color: #f16522; font-size: 16pt;"><u><span style="font-size: 14pt;">Предлагаем исключительно надежные страховые компаний с рейтингом А+ и выше.</span></u></span></b>
            </p>
            <p style="text-align: center;">
                <b><span style="color: #f16522; font-size: 16pt;"><u><span style="font-size: 14pt;"><br>
 </span></u></span></b>
            </p>
            <p style="text-align: center;">
                Покупая полис у надёжной компании, Вы исключаете риски отказа в выплате и долгую волокиту с получением компенсации.
            </p>
            <p style="text-align: center;">
                <img width="77" alt="13.png" src="/upload/medialibrary/54d/54d5249f933d92ca1525d1d5599c5227.png" height="67" title="13.png"><br>
            </p>
        </div>
    </div>
    <br/><br/>
    <div class="row text-center">
        <div class="col-md-4">
            <p style="text-align: center;">
                <span style="color: #f16522;"> </span><b><span style="font-size: 16pt;"><span style="color: #f16522;"><u><span style="font-size: 14pt;">Поможем получить диагностическую карту автомобиля.</span></u></span></span></b><span style="color: #f16522;"> </span>
            </p>
            <p>
                Поможем своим клиентам в минимально короткие сроки пройти техосмотр и получить диагностическую карту от аккредитованной СТО.
            </p>
            <p style="text-align: center;">
                <img width="77" alt="8.png" src="/upload/medialibrary/40c/40cdf7a54d385d6f872036c3917e68e6.png" height="67" title="8.png"><br>
            </p>
        </div>
        <div class="col-md-4">
            <p style="text-align: center;">
                <span style="color: #f16522;"><u><b><span style="font-size: 14pt;">Доставка полиса в день оформления или отправка электронного полиса</span></b></u></span>.
            </p>
            <p>
                Вы можете лично приехать за полисом в офис, либо произведём его доставку по удобному адресу, также сможем отправить электронный вариант на почту.
            </p>
            <p style="text-align: center;">
                <img width="77" alt="17.png" src="/upload/medialibrary/038/038060ffd596658d122defa58ba14e72.png" height="67" title="17.png"><br>
            </p>
        </div>
        <div class="col-md-4">
            <p style="text-align: center;">
                <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u><span style="font-size: 14pt;">Оплата наличными или банковской картой. Для юрлиц безналичный расчет</span></u></span></b></span><span style="font-size: 14pt;">. </span>
            </p>
            <p>
                Принимаем к оплате наличные и банковский карты, а для юридических лиц выставляем счёт на оплату полиса.&nbsp;
            </p>
            <p style="text-align: center;">
                <img width="77" alt="4.png" src="/upload/medialibrary/fcc/fccc178c835ac1bd9552477d89b119db.png" height="67" title="4.png"><br>
            </p>
        </div>
    </div>
    <p>
        <br>
    </p>
    <p>
        <br>
        <span style="font-size: 14pt;">С точки зрения закона полис ОСАГО обязан иметь каждый автовладелец. Он призван компенсировать ущерб третьим лицам, которые пострадали от Ваших действий в ДТП. Размер материальной компенсации на каждого пострадавшего ограничен 500&nbsp;000 рублей.</span>
    </p>
    <p>
        <br>
    </p>
    <p>
        <br>
    </p>
    <h2 align="center"><span style="color: #f16522;"><u>КАСКО.</u></span></h2>
    <u>
        <p style="text-align: center;">
            <span style="font-size: 16pt;"><b>Комплексное страхование автомобилей от всех видов рисков.</b></span>
        </p>
        <p style="text-align: center;">
 <span style="font-size: 16pt;"><b><br>
 </b></span>
        </p>
    </u>
    <div class="row text-center">
        <div class="col-sm-4">
            <p style="text-align: center;">
                <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u><span style="font-size: 14pt;">Широкий выбор программ страхования.</span></u></span></b></span>
            </p>
            <p style="text-align: left;">
                <span style="font-size: 14pt;">Предлагаем десятки программ страхования от ведущих страховых компаний России.</span>
            </p>
            <p style="text-align: center;">
                <img width="77" alt="11.png" src="/upload/medialibrary/4f1/4f13b3eb38b648dad52b0f35ec713a03.png" height="67" title="11.png"><br>
            </p>
        </div>
        <div class="col-sm-4">
            <p style="text-align: center;">
                <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u><span style="font-size: 14pt;">Скидки до 30% на оформление полиса КАСКО</span></u></span></b></span><span style="font-size: 14pt;">. </span>
            </p>
            <span style="font-size: 14pt;"> </span>
            <p>
                <span style="font-size: 14pt;">Сравниваем предложения от нескольких страховых компании в поисках наибольшей скидки для Вас</span>.
            </p>
            <p style="text-align: center;">
                <img width="77" alt="1.png" src="/upload/medialibrary/d5e/d5e41c7610e448fbfdc3f571c7f7f958.png" height="67" title="1.png"><br>
            </p>
        </div>
        <div class="col-sm-4">
            <p style="text-align: center;">
                <span style="font-size: 16pt;"><b><span style="color: #f16522; font-size: 14pt;"><u>Предлагаем исключительно надежные страховые компаний с рейтингом А+ и выше.</u></span></b></span>
            </p>
            <p>
                <span style="font-size: 14pt;">Покупая полис у надёжной компании, Вы исключаете риски отказа в выплате и долгую волокиту с получением компенсации.</span>
            </p>
            <p style="text-align: center;">
                <img width="77" alt="12.png" src="/upload/medialibrary/b15/b15e6171ce9bda1efc56503fa9982c6a.png" height="67" title="12.png"><br>
            </p>
        </div>
    </div><br/><br/>
    <div class="row text-center">
        <div class="col-sm-4">
            <p style="text-align: center;">
                <span style="font-size: 16pt;"><b><span style="color: #f16522; font-size: 14pt;"><u>Честный и прозрачный договор без скрытых платежей</u></span></b></span><span style="font-size: 14pt;"><u>.</u></span>
            </p>
            <p>
                <span style="font-size: 14pt;">Заключаем честный и прозрачный договор на покупку полиса КАСКО, без скрытых пунктов и дополнительных платежей.</span>
            </p>
            <p style="text-align: center;">
                <img width="77" alt="3.png" src="/upload/medialibrary/b72/b726b2adced29640f726e24571d88a04.png" height="67" title="3.png"><br>
            </p>
        </div>
        <div class="col-sm-4">
            <p style="text-align: center;">
                <b><span style="font-size: 16pt; color: #f16522;"><u><span style="font-size: 14pt;">Доставка полиса в день оформления или отправка электронного полиса.</span></u></span></b>
            </p>
            <p>
                <span style="font-size: 14pt;">Вы можете лично приехать за полисом в офис, либо произведём его доставку по удобному адресу, также сможем отправить электронный вариант на почту.</span>
            </p>
            <p style="text-align: center;">
                <img width="77" alt="17.png" src="/upload/medialibrary/038/038060ffd596658d122defa58ba14e72.png" height="67" title="17.png"><br>
            </p>
        </div>
        <div class="col-sm-4">
            <p style="text-align: center;">
                <span style="font-size: 16pt;"><b><span style="color: #f16522;"><u><span style="font-size: 14pt;">Оплата наличными или банковской картой. Для юрлиц безналичный расчет</span></u></span></b></span><span style="font-size: 14pt;">.</span>
            </p>
            <p>
                <span style="font-size: 14pt;">Принимаем к оплате наличные и банковский карты, а для юридических лиц выставляем счёт на оплату полиса.&nbsp;</span>
            </p>
            <p style="text-align: center;">
                <img width="77" alt="9.png" src="/upload/medialibrary/87e/87e1c16d579da70f4160ebd93bf9c8d4.png" height="67" title="9.png"><br>
            </p>
        </div>
    </div>
    <p>
        &nbsp;
    </p>
    <span style="font-size: 14pt;">Полис КАСКО позволит отремонтировать автомобиль в случае аварии или возместить материальный ущерб в случаи угона. Страхование осуществляется по двум основным направлениям: «Хищение» и «Повреждение». При этом любой покупатель сможет выбрать дополнительные опции, например, услугу аварийного комиссара, увеличение лимита гражданской ответственности. </span>
    <p>
 <span style="font-size: 14pt;">
	&nbsp; &nbsp;&nbsp;</span>
    </p>
    <h2><span style="font-size: 15pt; color: #f16522;"><b><u>Страхование автомобилей.</u></b></span></h2>

    <div class="box1">
        <h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><span style="font-size: 13pt;">Преимущества страхования у нас:</span></h3>
        <ul>
            <li class="li">Проведение технического осмотра и оформление договора ОСАГО в одном месте</li>
            <li class="li">Ведущие страховые компании: Росгосстрах, Ренесанс, Югория</li>
            <li class="li">Перенос скидок по базе РСА в связи с изменением данных в водительском удостоверении</li>
            <li class="li">Индивидуальный подход и приятные скидки при оформлении договора КАСКО</li>
            <li class="li">Страховой продукт "РЕМОНТ У ДИЛЕРА": не виноваты в ДТП – сразу в сервис официального дилера!</li>
        </ul>
        <br>
        Звоните прямо сейчас и получите бесплатную консультацию специалиста! <br>
        <br>
    </div>
    &nbsp;<img width="228" alt="strahovanie1.png" src="/upload/medialibrary/953/9533dd3df71bed2cb95e7c739e74e012.png" title="Страхование ОСАГО"><img width="228" alt="strahovanie2.png" src="/upload/medialibrary/32f/32fb979ec684d83e4b03c64844fb3275.png" title="Страхование КАСКО"><img width="228" alt="strahovanie3.png" src="/upload/medialibrary/3c3/3c356622961101e2cacc2c856e134179.png" title="Экономия на КАСКО и ОСАГО"><br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>