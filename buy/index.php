<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Купить");
?><div>
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"auto_request",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => "auto_request",
		"EDIT_URL" => "",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"LIST_URL" => "",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "/buy/",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
		"WEB_FORM_ID" => "7"
	)
);?>
</div>
 <!--
    <div class="block">
        <a href="#form_question" class="question btn-std">Задать вопрос</a>
    </div>
-->
<div id="form_question" style="display: none">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:form.result.new",
	"form_question",
	Array(
		"CACHE_TIME" => "3600",
		"CACHE_TYPE" => "A",
		"CHAIN_ITEM_LINK" => "",
		"CHAIN_ITEM_TEXT" => "",
		"COMPONENT_TEMPLATE" => "form_question",
		"EDIT_URL" => "result_edit.php",
		"IGNORE_CUSTOM_TEMPLATE" => "Y",
		"LIST_URL" => "result_list.php",
		"SEF_MODE" => "N",
		"SUCCESS_URL" => "",
		"USE_EXTENDED_ERRORS" => "Y",
		"VARIABLE_ALIASES" => array("WEB_FORM_ID"=>"WEB_FORM_ID","RESULT_ID"=>"RESULT_ID",),
		"WEB_FORM_ID" => "4"
	)
);?>
</div>
 <br>
<div class="question" id="question-manager" style="display: none">
	 <?$APPLICATION->IncludeComponent(
	"kreattika:forms.feedback",
	"feedback",
	Array(
		"AJAX_MODE" => "Y",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "Y",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CHECK_FIELD_EMAIL" => "N",
		"CHECK_FIELD_NAME" => "Y",
		"CHECK_FIELD_PHONE" => "Y",
		"CHECK_FIELD_TEXT" => "N",
		"COMPONENT_TEMPLATE" => "feedback",
		"EMAIL_TO" => "sales@dolcar.ru",
		"EVENT_MESSAGE_ID" => array(0=>"26",),
		"FIELD_EMAIL_TITLE" => "Email:",
		"FIELD_NAME_TITLE" => "Имя:",
		"FIELD_PHONE_TITLE" => "Телефон:",
		"FIELD_TEXT_TITLE" => "Текст:",
		"OK_TEXT" => "Ваше сообщение принято. Наши менеджеры свяжутся с Вами в самое ближайшее время. Удачного дня.",
		"SUBMIT_TITLE" => "Отправить",
		"USE_CAPTCHA" => "Y",
		"USE_FIELD_EMAIL" => "Y",
		"USE_FIELD_NAME" => "Y",
		"USE_FIELD_PHONE" => "Y",
		"USE_FIELD_TEXT" => "Y"
	)
);?>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>