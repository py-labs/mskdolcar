<?php

if(!isset($amo_no_redirect)){
    header( 'Location: http://'.$_SERVER['HTTP_HOST'], true, 301 );
}

class amoSend
{
    private $subdomain;
    private $login;
    private $user_hash;

    function __construct ($subdomain, $login, $user_hash){

            $this->subdomain 	= $subdomain;
            $this->login 		= $login;
            $this->user_hash 	= $user_hash;
    }

    public function GetAccountInfo()
    {
        $url = 'https://' . $this->subdomain . '.amocrm.ru/private/api/v2/json/accounts/current?USER_HASH=' . $this->user_hash . '&USER_LOGIN=' . $this->login;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, dirname(__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_COOKIEJAR, dirname(__FILE__) . '/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $out = curl_exec($curl);
        $code = (int)curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);


        $errors = [
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        ];

        try {
            if ($code != 200 && $code != 204)
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            else

                return json_decode($out, true);

        } catch (Exception $E) {
            echo $res = 'Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode();

        }
    }

        public function ShowDump($obj = ''){

            if (!$obj){
                echo 'ERROR : ARGUMENT IS EMPTY!';
                return false;
            }else{
                    switch($obj){
                     case 'account' :  print_r($this->GetAccountInfo());	break;
                    default : echo 'ERROR: UNDEFINED ARGUMENT!';
                    }
            }
        }

        public function setLead($params){

            if(!is_array($params) || empty($params)){ return false; }
            if(!isset($params['name']) || !isset($params['status_id']) ){ return false; }

            $leads['request']['leads']['add']=[
                [
                    'name'=> $params['name'].' ( ' .  date("m.d.y") .' ' . date("H:i:s") .' )',
                    'status_id'=> $params['status_id']  ,
                    'price'=> $params['price'] ? $params['price'] : 0 ,
                    'responsible_user_id'=>$params['responsible_user_id'] ? $params['responsible_user_id'] : '',
                    'tags' => $params['tag'] ? $params['tag'] : 'SITE' ,
                    'custom_fields'=> $params['custom_fields'] ? $params['custom_fields'] : [] ,
                ]
            ];

            $url = 'https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/leads/set?USER_HASH='.$this->user_hash.'&USER_LOGIN='.$this->login;
            $curl=curl_init();
            curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
            curl_setopt($curl,CURLOPT_URL,$url);
            curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
            curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($leads));
            curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
            curl_setopt($curl,CURLOPT_HEADER,false);
            curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
            curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
            curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

            $out=curl_exec($curl);
            $code=(int)curl_getinfo($curl,CURLINFO_HTTP_CODE);

            if($code!=200 && $code!=204){ return false; }

            $response = json_decode($out, true);

            if(isset($response['response']['leads']['add']['0']['id'])){ Log::write("__3__\n");
              return $response['response']['leads']['add']['0']['id'];
            }else{
               return false;
            }
      }



    public function setContact($params){

      if(!is_array($params) || empty($params)){ return false; }
      if( !isset($params['name']) ){ return false; }

      $contacts['request']['contacts']['add'] =  [
          [
              "name" => $params['name'],
              "responsible_user_id" => $params['responsible_user_id']? $params['responsible_user_id'] : '' ,
              "linked_leads_id"=>  $params['linked_leads_id'] ? $params['linked_leads_id'] : '',
              'tags' => $params['tag'] ? $params['tag'] : 'SITE' ,
              'custom_fields'=> $params['custom_fields'] ? $params['custom_fields'] : [] ,
          ]
      ];


      $url = 'https://'.$this->subdomain.'.amocrm.ru/private/api/v2/json/contacts/set?USER_HASH='.$this->user_hash.'&USER_LOGIN='.$this->login;
      $curl=curl_init(); #Сохраняем дескриптор сеанса cURL
      #Устанавливаем необходимые опции для сеанса cURL
      curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
      curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-API-client/1.0');
      curl_setopt($curl,CURLOPT_URL,$url);
      curl_setopt($curl,CURLOPT_CUSTOMREQUEST,'POST');
      curl_setopt($curl,CURLOPT_POSTFIELDS,json_encode($contacts));
      curl_setopt($curl,CURLOPT_HTTPHEADER,array('Content-Type: application/json'));
      curl_setopt($curl,CURLOPT_HEADER,false);
      curl_setopt($curl,CURLOPT_COOKIEFILE,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
      curl_setopt($curl,CURLOPT_COOKIEJAR,dirname(__FILE__).'/cookie.txt'); #PHP>5.3.6 dirname(__FILE__) -> __DIR__
      curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,0);
      curl_setopt($curl,CURLOPT_SSL_VERIFYHOST,0);

      $out=curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
      $code=curl_getinfo($curl,CURLINFO_HTTP_CODE);

      $code=(int)curl_getinfo($curl,CURLINFO_HTTP_CODE);

      if($code!=200 && $code!=204){ return false; }

      $response = json_decode($out, true);

      if(isset($response['response']['contacts']['add']['0']['id'])){
        return (int)$response['response']['contacts']['add']['0']['id'];
      }else{
         return false;
      }
  }


}
