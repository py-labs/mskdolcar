/**
 * Created by pathfinder on 13.06.17.
 */
function shownum(n, d, s) {
    if (arguments.length == 2) {
        s = " ";
    }
    if (arguments.length == 1) {
        s = " ";
        d = ".";
    }
    n = n.toString();
    a = n.split(d);
    x = a[0];
    y = a[1];
    z = "";
    if (typeof(x) != "undefined") {
        for (i = x.length - 1; i >= 0; i--)
            z += x.charAt(i);
        z = z.replace(/(\d{3})/g, "$1" + s);
        if (z.slice(-s.length) == s)
            z = z.slice(0, -s.length);
        x = "";
        for (i = z.length - 1; i >= 0; i--)
            x += z.charAt(i);
        if (typeof(y) != "undefined" && y.length > 0)
            x += d + y;
    }
    return x;
}


function dotime() {
    var timecredit = document.forms.calc.time.value;
    document.forms.calc.timehint.value = timecredit;

    var testio = document.forms.calc.timehint.value;
    console.log('это мы и пишем', timecredit);
    console.log('это мы и получаем', testio);

}

function docalc() {
    var rsum = document.forms.calc.summcredit.value;
    var lsum = document.forms.calc.showsum.value;
    rsum = rsum.replace(' ', '');
    var rtime = document.forms.calc.time.value;
    rtime = rtime.replace(' ', '');
    var rate = 15;
    var term = document.forms.calc.time.value;

//Эти значения можно поменять
    if (rsum <= 300000) {
        document.forms.calc.timehint.value = '';
    }
    //if (rsum > 300000 & rsum <= 400000){document.forms.calc.timehint.value = '(от 24 месяцев)';}
    if (rsum > 400000) {
        document.forms.calc.timehint.value = '';
    }
//делаем из годовой ставки месячную
    //stavka = stavka / 12;
    plat = rsum / rtime;
    // var result = (plat/(stavka/100));
    //result = Math.floor(result);
    //document.forms.calc.res.value = result;

    var month_rate = (rate / 100 / 12);   //  месячная процентная ставка по кредиту (= годовая ставка / 12)
    var k = (month_rate * Math.pow((1 + month_rate), term)) / ( Math.pow((1 + month_rate), term) - 1  ); // коэффициент аннуитета

    var payment = Math.round(k * rsum, 3);   // Размер ежемесячных выплат

    console.log(rsum);
    console.log(rate, 'Процентная ставка');
    console.log(term, 'Срок на который выдается кредит');
    console.log(payment);

    document.forms.calc.res.value = payment;


}


function doinitsum() {
    var rsum = document.forms.calc.initial.value;
    document.forms.calc.initialrange.value = rsum;

}


function oformsum() {
    rsum = document.forms.calc.sum.value;
    rsum = rsum.replace(' ', '');
    document.forms.calc.sum.value = shownum(rsum);
    document.forms.calc.res.value = shownum(document.forms.calc.res.value);
}

function summkredit() {
    otl1 = document.forms.calc.showsum.value;
    otl2 = document.forms.calc.initialrange.value;
    rsum = (document.forms.calc.showsum.value) - (document.forms.calc.initialrange.value);
    //console.log(otl1);
    //console.log(otl2);
    //console.log(rsum);
    document.forms.calc.summcredit.value = shownum(rsum);

}

//данные для ползунков


$(document).ready(function () {
    rsum = (document.forms.calc.showsum.value) - (document.forms.calc.initialrange.value);
    document.forms.calc.summcredit.value = shownum(rsum);
    //исполняемый код после загрузки документа
});


$(document).ready(function () {
    var rsum = document.forms.calc.summcredit.value;
    rsum = rsum.replace(' ', '');

    var rate = 15;
    var term = 12;


    var month_rate = (rate / 100 / 12);   //  месячная процентная ставка по кредиту (= годовая ставка / 12)
    var k = (month_rate * Math.pow((1 + month_rate), term)) / ( Math.pow((1 + month_rate), term) - 1  ); // коэффициент аннуитета

    var payment = Math.round(k * rsum, 3);   // Размер ежемесячных выплат


    document.forms.calc.res.value = payment;
    //исполняемый код после загрузки документа

    var slideout = new Slideout({
        'panel': document.getElementById('main'),
        'menu': document.getElementById('sf-menu'),
        'padding': 256,
        'tolerance': 70
    });

    // Toggle button
    document.querySelector('.open-menu').addEventListener('click', function() {
        slideout.toggle();
    });
    document.querySelector('.close-menu').addEventListener('click', function() {
        slideout.toggle();
    });
    slideout.disableTouch();

});


$("#initial").change(function () {
    console.log('test');
    if ($(this).val() != "") {
        console.log('должно заполнятся');

        var valforslider = document.forms.calc.initial.value;

        //$( "#initial" ).val( $( "#initialrange" ).slider( "value" ) );
        $("#initialrange").slider("value", valforslider);


        document.forms.calc.initialrange.value = document.forms.calc.initial.value;
    }

    if ($(this).val() == "") {
        console.log('Инпут пуст или меньше косаря');
        document.forms.calc.initial.value = document.forms.calc.initialrange.value;
    }

    if ($(this).val() < 10000) {
        document.forms.calc.initial.value = 10000;
        document.forms.calc.initialrange.value = 10000;


    }


});


$("#time").change(function () {
    console.log('Изменяем timehint');
    if ($(this).val() != "") {
        console.log('должно заполняться');

        if ($(this).val() < 6) {
            document.forms.calc.timehint.value = 6;
            document.forms.calc.time.value = 6;
        }
        else {
            document.forms.calc.timehint.value = document.forms.calc.time.value;
        }

        var valforslidertime = document.forms.calc.timehint.value;

        //$( "#initial" ).val( $( "#initialrange" ).slider( "value" ) );
        $("#timerange").slider("value", valforslidertime);


    }

    if ($(this).val() == "") {
        console.log('Инпут пуст или меньше косаря');
        document.forms.calc.timehint.value = document.forms.calc.time.value;
    }


});


$(function () {
    otl1 = document.forms.calc.showsum.value;
    otl1 = parseInt(otl1);
    console.log(otl1);

    $("#initialrange").slider({
        range: "max",
        min: 10000,
        max: otl1,
        step: 1000,
        value: 10000,
        slide: function (event, ui) {
            $("#initial").val(ui.value);
            doinitsum();
            summkredit();
            docalc();
        }
    });
    $("#initial").val($("#initialrange").slider("value"));

    $("#sumrange").slider({
        range: "max",
        min: 100000,
        max: 1000000,
        step: 1000,
        value: 200000,
        slide: function (event, ui) {
            $("#sum").val(ui.value);
            docalc();
            oformsum();
        }
    });
    $("#sum").val($("#sumrange").slider("value"));

    $("#timerange").slider({

        range: "max",
        min: 1,
        max: 36,
        step: 1,
        value: 6,
        slide: function (event, ui) {
            $("#time").val(ui.value);
            dotime();
            docalc();
            oformsum();
        }
    });
    $("#time").val($("#timerange").slider("value"));


});


$(document).ready(function () {

    $(".auto-request").validate({

        rules: {

            form_text_49: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
            form_text_50: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
        },
    });


    $(".auto-sale").validate({

        rules: {

            form_text_72: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
            form_text_73: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
        },
    });


    $(".auto-credit").validate({

        rules: {

            form_text_196: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
            form_text_197: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },

            myname: {
                required: true
            }
        }


    });

    $("#calc").validate({

        rules: {

            form_text_196: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
            form_text_197: {
                required: true,
                minlength: 4,
                maxlength: 16,
            },
        },


    });


});