<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

    <!-- fotorama.css & fotorama.js. -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script><?php
/* @class CBitrixComponentTemplate */
/**
 * 0 => string 'arResult' (length=8)
 * 1 => string 'arParams' (length=8)
 * 2 => string 'parentTemplateFolder' (length=20)
 * 3 => string 'APPLICATION' (length=11)
 * 4 => string 'USER' (length=4)
 * 5 => string 'DB' (length=2)
 * 6 => string 'templateName' (length=12)
 * 7 => string 'templateFile' (length=12)
 * 8 => string 'templateFolder' (length=14)
 * 9 => string 'componentPath' (length=13)
 * 10 => string 'component' (length=9)
 * 11 => string 'templateData' (length=12)
 **/
#var_dump(array_keys(get_defined_vars()));
?>
    <div class="carblock">
        <div class="row">
            <div class="col-lg-8 col-md-7 col-sm-6">
                <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-ratio="4/3" data-loop="true">
                    <? foreach ($arResult["PROPERTIES"]["ADDITIONAL_IMAGES"]["VALUE"] as $pic):
                        $src = CFile::GetPath($pic);
                        {
                            ?>
                            <a href="<?= $src ?>" data-full="<?
                            $src ?>"><img
                                        class="detail_picture"
                                        src="<?
                                        $src ?>"
                                        border="0"
                                        width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>"
                                        height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>"
                                        alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>"
                                        title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>"
                                >
                            </a>

                            <?
                        }
                    endforeach;
                    ?><img
                            class="detail_picture"
                            border="0"
                            src="<?= $arResult["DETAIL_PICTURE"]["SRC"] ?>"
                            width="<?= $arResult["DETAIL_PICTURE"]["WIDTH"] ?>"
                            height="<?= $arResult["DETAIL_PICTURE"]["HEIGHT"] ?>"
                            alt="<?= $arResult["DETAIL_PICTURE"]["ALT"] ?>"
                            title="<?= $arResult["DETAIL_PICTURE"]["TITLE"] ?>"
                    /><?
                    ?>
                </div>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-6">
                <div class="element_params">
                    <div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><h3><?= $arResult["NAME"] ?></h3></div>
                    <div class="element_line year-detail"><p><?php echo GetMessage('MAX_YEAR') ?></p><span><?php echo $arResult["PROPERTIES"]["YEAR"]["VALUE"] ?></span></div>
                    <div class="element_line"><p><?php echo GetMessage('MAX_DISTANCE') ?></p> <span><?php echo $arResult["PROPERTIES"]["MILEAGE"]["VALUE"] ?></span></div>
                    <div class="element_line"><p><?php echo GetMessage('MAX_ENGINE_TYPE') ?></p> <span><?php echo $arResult["PROPERTIES"]["TYPE_ENGINE"]["VALUE"] ?></span></div>
                    <!--<div class="element_line" ><p><?php /*echo GetMessage('MAX_ENGINE_VOLUME') */ ?></p> <span><?php /*echo $arResult["PROPERTIES"]["ENGINE_VOLUME"]["VALUE"] */ ?>&nbsp;см&#179;<?php /*echo ($arResult['ENGINE_POWER'] ? sprintf('&nbsp;(%s&nbsp;л.с.)', $arResult['ENGINE_POWER']) : '') */ ?></span></div>-->
                    <!-- <div class="element_line" ><p><?php /*echo GetMessage('MAX_DRIVE_TYPE') */ ?></p> <span><?php /*echo $arResult['DRIVE_TYPE'] */ ?></span></div>
            <div class="element_line" ><p><?php /*echo GetMessage('MAX_GEARBOX_TYPE') */ ?></p> <span><?php /*echo $arResult['GEARBOX_TYPE'] */ ?></span></div>
            <div class="element_line" ><p><?php /*echo GetMessage('MAX_WHEEL_PLACE') */ ?></p> <span><?php /*echo $arResult['WHEEL_PLACE'] */ ?></span></div>-->
                    <?php if ('Y' == $arParams['SHOW_COLOR']) : ?>
                        <div class="element_line"><p><?php echo GetMessage('MAX_BODY_COLOR') ?></p> <span><?php echo $arResult['BODY_COLOR'] ?></span></div>
                    <?php endif ?>
                    <?php if ('Y' == $arParams['SHOW_CONDITION']) : ?>
                        <div class="element_line"><p><?php echo GetMessage('MAX_CONDITION') ?></p> <span><?php echo $arResult['CONDITION'] ?></span></div>
                    <?php endif ?>
                    <?php if ('Y' == $arParams['SHOW_PTS']) : ?>
                        <div class="element_line"><p><?php echo GetMessage('MAX_PTS_OWNERS') ?></p> <span><?php echo $arResult['PTS_OWNERS'] ?></span></div>
                    <?php endif ?>
                    <?php if ('Y' == $arParams['SHOW_CUSTOMS']) : ?>
                        <div class="element_line"><p><?php echo GetMessage('MAX_PRICE_NO_CUSTOMS') ?></p> <span><?php
                                echo($arResult['PRICE_NO_CUSTOMS'] ? GetMessage('MAX_PRICE_NO_CUSTOMS_FALSE') : GetMessage('MAX_PRICE_NO_CUSTOMS_TRUE'));
                                ?></span></div>
                    <?php endif ?>

                    <div class="element_line price-detail"><p><?php echo GetMessage('MAX_PRICE') ?></p> <span><span class="element_price"><?php echo $arResult["PROPERTIES"]["PRICE"]["VALUE"] ?> р.</span></span></div>


                    <?php if ('Y' == $arParams['SHOW_AVAILABILITY']) : ?>
                        <div class="element_line"><p><?php echo GetMessage('MAX_AVAILABILITY') ?></p> <span><?php echo $arResult['AVAILABILITY'] ?></span></div>
                    <?php endif ?>

                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('.fancybox').fancybox();
                        });
                    </script>
                    <div class="additional-info">
                        <h5>Заинтересовал автомобиль?</h5>
                        <p>Специалист отдела продаж ответит на любые вопросы по автомобилю.</p>
                        <a href="#form_tel" class="btn-std callback" style="float: left">Заказать звонок</a>
                        <a href="tel:+7(495)25-500-25"><h4 class="tel_cart">+7(495)25-500-25</h4></a>
                    </div>
                </div>
            </div>


            <?php
            $i = 0;
            foreach ($arResult['PHOTOS'] as $photo) :
                if ($i < 1) {
                    $GLOBALS["image_credit"] = $photo['BIG']['1'];
                }
                $i++;
            endforeach ?>


        </div>

        <? if ($arResult["PROPERTIES"]["CHARACTERISTICS"]["VALUE"]): ?>
        <div class="box1">
            <h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><?php echo GetMessage('MAX_OPTIONS_TITLE') ?></h3>
        </div>
        <div class="row">
            <? foreach ($arResult["PROPERTIES"]["CHARACTERISTICS"]["VALUE"] as $arItem): ?>
                <div class="grid_3">
                <span class="li">
                    <?= $arItem ?>
                </span>
                </div>
            <? endforeach; ?>
            <? endif; ?>
        </div>

        <div class="box1">
            <h3><?php echo GetMessage('MAX_DESCRIPTION') ?></h3>
            <p><?php echo $arResult["DETAIL_TEXT"] ?></p>
        </div>


        <?php if ('Y' == $arParams['SHOW_DESCRIPTION'] && !empty($arResult['DESCRIPTION'])) : ?>
            <div class="box1">
                <h3><?php echo GetMessage('MAX_DESCRIPTION') ?></h3>
                <p><?php echo $arResult['DESCRIPTION'] ?></p>
            </div>
        <?php endif ?>
        <?php if ('Y' == $arParams['SHOW_INSPECTION'] && !empty($arResult['INSPECTION'])) : ?>
            <div class="box1">
                <h3><?php echo GetMessage('MAX_INSPECTION') ?></h3>
                <p><?php echo $arResult['INSPECTION'] ?></p>
            </div>
        <?php endif ?>

        <?php if (('Y' == $arParams['SHOW_CONTACTS']) && (bool)$arResult['PHONES']) : ?>
            <div class="element_description">
                <h3><?php echo GetMessage('MAX_CONTACTS') ?></h3>
                <p><?php echo implode(';<br />', $arResult['PHONES']) ?></p>
            </div>
        <?php endif ?>
        <div class="clearfix"></div>
    </div>


<? /*
<div class="news-detail">
	<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
		<img
			class="detail_picture"
			border="0"
			src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
			width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
			height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
			alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
			title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
			/>
	<?endif?>
	<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
		<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
	<?endif;?>
	<?if($arParams["DISPLAY_NAME"]!="N" && $arResult["NAME"]):?>
		<h3><?=$arResult["NAME"]?></h3>
	<?endif;?>
	<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
		<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
	<?endif;?>
	<?if($arResult["NAV_RESULT"]):?>
		<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
		<?echo $arResult["NAV_TEXT"];?>
		<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
	<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
		<?echo $arResult["DETAIL_TEXT"];?>
	<?else:?>
		<?echo $arResult["PREVIEW_TEXT"];?>
	<?endif?>
	<div style="clear:both"></div>
	<br />
	<?foreach($arResult["FIELDS"] as $code=>$value):
		if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
			if (!empty($value) && is_array($value))
			{
				?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
			}
		}
		else
		{
			?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
		}
		?><br />
	<?endforeach;
	foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

		<?=$arProperty["NAME"]?>:&nbsp;
		<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
			<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
		<?else:?>
			<?=$arProperty["DISPLAY_VALUE"];?>
		<?endif?>
		<br />
	<?endforeach;
	if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
	{
		?>
		<div class="news-detail-share">
			<noindex>
			<?
			$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
					"HANDLERS" => $arParams["SHARE_HANDLERS"],
					"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
					"PAGE_TITLE" => $arResult["~NAME"],
					"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
					"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
					"HIDE" => $arParams["SHARE_HIDE"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);
			?>
			</noindex>
		</div>
		<?
	}
	?>
</div>

*/ ?>