<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<? if (!empty($arResult)): ?>
    <div id="stuck_container">
        <div class="width-wrapper container">
            <nav class="sf-menu-nav">
                <a href="#" class="open-menu"><i class="fa fa-bars"></i></a>
                <div id="sf-menu">
                    <a href="#" class="close-menu"><i class="fa fa-times"></i></a>
                    <ul class="sf-menu">
                        <?
                        $previousLevel = 0;
                        foreach ($arResult

                        as $arItem): ?>

                        <? if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel): ?>
                            <?= str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"])); ?>
                        <? endif ?>

                        <? if ($arItem["IS_PARENT"]): ?>

                        <li<? if ($arItem["SELECTED"]): ?> class="selected"<? endif ?>><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a>
                            <ul class="multi">

                                <? else: ?>

                                    <? if ($arItem["PERMISSION"] > "D"): ?>

                                        <li<? if ($arItem["SELECTED"]): ?> class="selected"<? endif ?>><a href="<?= $arItem["LINK"] ?>"><?= $arItem["TEXT"] ?></a></li>

                                    <? else: ?>

                                        <li<? if ($arItem["SELECTED"]): ?> class="selected"<? endif ?>><a href=""" title="<?= GetMessage("MENU_ITEM_ACCESS_DENIED") ?>"><?= $arItem["TEXT"] ?></a></li>

                                    <? endif ?>

                                <? endif ?>

                                <? $previousLevel = $arItem["DEPTH_LEVEL"]; ?>

                                <? endforeach ?>

                                <? if ($previousLevel > 1)://close last item tags?>
                                    <?= str_repeat("</ul></li>", ($previousLevel - 1)); ?>
                                <? endif ?>

                            </ul>
                </div>
            </nav>
            <? /*$APPLICATION->IncludeComponent("bitrix:search.form", "menu_search", Array(
	"COMPONENT_TEMPLATE" => "flat",
		"PAGE" => "#SITE_DIR#search/index.php",	// Страница выдачи результатов поиска (доступен макрос #SITE_DIR#)
		"USE_SUGGEST" => "N",	// Показывать подсказку с поисковыми фразами
	),
	false
);*/ ?>
            <div class="clearfix"></div>
        </div>
    </div>

    <div id="stuck_container">
        <div class="width-wrapper container">

            <? $APPLICATION->IncludeComponent(
                "maxposter:api.filter",
                "dolcar",
                array(
                    "COMPONENT_TEMPLATE" => ".default",
                    "MAX_API_LOGIN" => "267",
                    "MAX_API_PASSWORD" => "dolauto267",
                    "URL_TEMPLATES_INDEX" => "/avtomobili-s-probegom/",
                    "SET_STATUS_404" => "N",
                    "SET_TITLE" => "Y"
                ),
                false
            ); ?>
            <div class="clearfix"></div>
        </div>
    </div>


<? endif ?>