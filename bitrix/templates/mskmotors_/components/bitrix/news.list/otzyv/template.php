<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>

    <style>
        .block {
            padding: 20px;
            position: relative;
            border: none
        }

        .block::after {
            content: '';
            position: absolute; /* Абсолютное позиционирование */
            left: 20px;
            bottom: 99px; /* Положение треугольника */
            border: 20px solid transparent;
            border-bottom: 20px solid #e5e5e5
        }

        .message-body p {
            font-weight: 100;
        }

        .message-body-answer p {
            font-weight: 100;
        }

        .message-body-answer {
            max-height: 100px !important;
        }

        #admin {
            font-weight: 500;
        }

        .review {
            background-color: white;
        }

        .message-header {
            background-color: #ed642f;
        }

        .answer {
            background-color: #e5e5e5;
        }

        .is-success {
            float: right;
        }

        .terms-check {
            margin-left: 23px;
        }

        .form-review {
            margin-bottom: 70px;
        }

        .entry {
            margin-bottom: 30px;
        }
    </style>
    <div class="box1">
        <div id="reviews">
            <div v-for="item in items" class="entry">
                <article class="message">
                    <div class="message-header">
                        <p>{{ item.NAME }}</p>
                        <p>{{ item.ACTIVE_FROM }}</p>
                    </div>
                    <div class="message-body review">
                        <p v-html="item.PREVIEW_TEXT"></p>
                    </div>
                </article>
                <article v-if="item.PROPERTIES.REVIEW_RESPONSE.VALUE.TEXT" class="message answer">
                    <div class="message-body-answer block">
                        <div><p id="admin">Долкар Авто</p></div>
                        <p>{{ item.PROPERTIES.REVIEW_RESPONSE.VALUE.TEXT }}</p>
                    </div>
                </article>
            </div>
            <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
                <br/><?= $arResult["NAV_STRING"] ?>
            <? endif; ?>
        </div>
    </div>

</div>

<script>
    let items = <?=json_encode($arResult["ITEMS"])?>;
    console.log(items);

    const reviews = new Vue({
        el: '#reviews',
        data: items,
        store,
    });
</script>
