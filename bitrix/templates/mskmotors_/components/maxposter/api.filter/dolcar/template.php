﻿<?
/* @class CBitrixComponentTemplate */
/**
 * 0 => string 'arResult' (length=8)
 * 1 => string 'arParams' (length=8)
 * 2 => string 'parentTemplateFolder' (length=20)
 * 3 => string 'APPLICATION' (length=11)
 * 4 => string 'USER' (length=4)
 * 5 => string 'DB' (length=2)
 * 6 => string 'templateName' (length=12)
 * 7 => string 'templateFile' (length=12)
 * 8 => string 'templateFolder' (length=14)
 * 9 => string 'componentPath' (length=13)
 * 10 => string 'component' (length=9)
 * 11 => string 'templateData' (length=12)
 **/

//var_dump($arResult["SELECTED"]['price']);


?>

<div class="banner3">
    <h2 style="display: none;" class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">Поиск авто</h2>
    <form method="POST" action="<?php echo $arResult['URL'] ?>" id="max-filter" class="bookingForm1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
        <div class="wrapper">
            <div class="row">
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <a id="allauto" class="btn-big btn-maxposter" href="/avtomobili-s-probegom/">Все авто</a>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="block">
                        <span for="<?php echo $arResult['FORM']['[mark_id]']['ID'] ?>" class="heading">Марка</span>
                        <select name="FS<?php echo $arResult['FORM']['[mark_id]']['NAME'] ?>" id="<?php echo $arResult['FORM']['[mark_id]']['ID'] ?>" class="tmSelect auto">
                            <option>Выберите марку</option>
                            <?php foreach ($arResult['FORM']['[mark_id]']['OPTIONS'] as $option) : ?>
                                <option value="<?php echo $option['NAME'] ?>"<?php echo($arResult['SELECTED']['mark_id'] == $option['NAME'] ? ' selected="selected"' : '') ?>><?php echo $option['VALUE'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="block">
                        <span for="<?php echo $arResult['FORM']['[model_id]']['ID'] ?>" class="heading">Модель</span>
                        <select name="FS<?php echo $arResult['FORM']['[model_id]']['NAME'] ?>" id="<?php echo $arResult['FORM']['[model_id]']['ID'] ?>" class="tmSelect auto">
                            <option>Выберите модель</option>
                            <?php foreach ($arResult['FORM']['[model_id]']['OPTIONS'] as $option) : ?>
                                <option value="<?php echo $option['NAME'] ?>"<?php echo($arResult['SELECTED']['model_id'] == $option['NAME'] ? ' selected="selected"' : '') ?> class="mark-<?php echo $option['PARENT'] ?>"><?php echo $option['VALUE'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-6">
                    <div class="block">
                        <span for="<?php echo $arResult['FORM']['[gearbox_type]']['ID'] ?>" class="heading">КПП</span>
                        <select name="FS<?php echo $arResult['FORM']['[gearbox_type]']['NAME'] ?>" id="<?php echo $arResult['FORM']['[gearbox_type]']['ID'] ?>" class="tmSelect auto">
                            <option></option>
                            <?php foreach ($arResult['FORM']['[gearbox_type]']['OPTIONS'] as $option) : ?>
                                <? var_dump($option); ?>

                                <option value="<?php echo $option['NAME'] ?>"<?php echo($arResult['SELECTED']['gearbox_type'] == $option['NAME'] ? ' selected="selected"' : '') ?>><?php echo $option['VALUE'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-6">
                    <div class="block2">
                        <span for="FS<?php echo $arResult['FORM']['[year][from]']['ID'] ?>" class="heading">Год от</span>
                        <input type="text"
                               size="9"
                               name="FS<?php echo $arResult['FORM']['[year][from]']['NAME'] ?>"
                               value="<?php echo $arResult['SELECTED']['year']['from'] ?>"
                               id="FS<?php echo $arResult['FORM']['[year][from]']['ID'] ?>"/>
                    </div>
                </div>
                <div class="col-md-1 col-sm-2 col-xs-6">
                    <div class="block2">
                        <span for="FS<?php echo $arResult['FORM']['[year][to]']['ID'] ?>" class="heading">до</span>
                        <input
                                type="text"
                                size="9"
                                name="FS<?php echo $arResult['FORM']['[year][to]']['NAME'] ?>"
                                value="<?php echo $arResult['SELECTED']['year']['to'] ?>"
                                id="FS<?php echo $arResult['FORM']['[year][to]']['ID'] ?>"/>
                    </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                    <div class="block" style="display: block;width: 100%;">
                        <input type="submit" value="Искать" class="btn-big btn-maxposter"/>

                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </form>
</div>