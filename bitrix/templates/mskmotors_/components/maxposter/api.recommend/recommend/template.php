<?php
/* @class CBitrixComponentTemplate */
/**
 * 0 => string 'arResult' (length=8)
 * 1 => string 'arParams' (length=8)
 * 2 => string 'parentTemplateFolder' (length=20)
 * 3 => string 'APPLICATION' (length=11)
 * 4 => string 'USER' (length=4)
 * 5 => string 'DB' (length=2)
 * 6 => string 'templateName' (length=12)
 * 7 => string 'templateFile' (length=12)
 * 8 => string 'templateFolder' (length=14)
 * 9 => string 'componentPath' (length=13)
 * 10 => string 'component' (length=9)
 * 11 => string 'templateData' (length=12)
 **/


?>

<div class="wrapper11">
    <?php if ($arResult['VEHICLES']) : ?>
        <div>
            <h3>Похожие автомобили</h3>
        </div>

        <? $iautos = 0 ?>
        <div class="row">
            <? foreach ($arResult['VEHICLES'] as $position => $arVehicle) : ?>
                <div class="col-md-3 col-sm-4">
                    <div class="box1">
                        <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                            <a href="<?= $arVehicle['URL_TO_VEHICLE'] ?>"><?= $arVehicle['YEAR'] ?> <?= $arVehicle['MARK']['NAME'] ?> <?= ((18 < mb_strlen($arVehicle['MODEL']['NAME']) && $pos = mb_strpos($arVehicle['MODEL']['NAME'], '(', 5)) ? trim(mb_substr($arVehicle['MODEL']['NAME'], 0, $pos)) : $arVehicle['MODEL']['NAME']) ?></a>
                        </h4>
                        <? if ($arVehicle['PHOTO']) : ?>
                            <a href="<?= $arVehicle['URL_TO_VEHICLE'] ?>"><img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="<?= $arVehicle['PHOTO']['0']['MED']['1']; ?>" alt="<?= htmlspecialchars($arVehicle['MARK']['NAME']), ' ', htmlspecialchars($arVehicle['MODEL_NAME']); ?>" width="270" height="202"/></a>
                        <? else : ?>
                            <a href="<?= $arVehicle['URL_TO_VEHICLE'] ?>"><img src="/bitrix/images/maxposter/no-image-tn.png" width="270" height="202"/></a>
                        <? endif ?>
                        <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                            <span class="first-left">Пробег <span class="highlighted"><?= number_format($arVehicle['DISTANCE'], '0', '.', ' '), ' ', ('km' == $arVehicle['DISTANCE_UNIT'] ? 'км' : 'миль'); ?></span></span>
                            <span class="first-right">Год <span class="highlighted"><?= $arVehicle['YEAR'] ?></span></span>
                            <span class="second-left">Двигатель <span class="highlighted"><?= $arVehicle['ENGINE_VOLUME'] ?>&nbsp;см&#179;</span></span>
                            <span class="second-left">КПП <span class="highlighted"><?= $arVehicle['GEARBOX'] ?></span></span>
                            <div class="clearfix"></div>
                        </div>
                        <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                            <div class="price">
                                <? if ($arVehicle['PRICE_NEW'] != 0): ?>
                                    <span class="second cur-price"><?= maxPrice($arVehicle['PRICE_NEW'], $arVehicle['PRICE_UNIT']) ?></span>
                                    <span class="second old-price"><?= maxPrice($arVehicle['PRICE_BASE'], $arVehicle['PRICE_UNIT']) ?></span>

                                <? else: ?>
                                    <span class="second"><?= maxPrice($arVehicle['PRICE'], $arVehicle['PRICE_UNIT']) ?></span>
                                <? endif; ?>
                            </div>
                            <a class="btn-orange-small" href="<?= $arVehicle['URL_TO_VEHICLE'] ?>">
                                <span>Подробнее</span>
                            </a>
                            <a href="#calculator_list" class="btn-orange-small credit-button-list fancybox">Кредит</a>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="wrapper11-bottom"></div>
            <?php endforeach ?>
        </div>
    <?php endif ?>
</div>


