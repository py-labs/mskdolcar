﻿<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<head>
  <title><?$APPLICATION->ShowTitle()?></title>
  <?$APPLICATION->ShowHead();?>
  <meta charset="utf-8">
  <meta name = "format-detection" content = "telephone=no" />
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">
  <link rel="stylesheet" href="css/grid.css">
  <link rel="stylesheet" href="css/search.css">
  <link rel="stylesheet" href="css/camera.css">
  <link rel="stylesheet" href="css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
  <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
  <script src="js/camera.js"></script>
  <script src="js/jquery.equalheights.js"></script>
  <script src="js/search.js"></script>
  <script src="booking/js/booking.js"></script>
  <script src="booking/js/jquery.placeholder.js"></script>
  <!--[if (gt IE 9)|!(IE)]><!-->
  <script src="js/jquery.mobile.customized.min.js"></script>
  <script src="js/wow.js"></script>
  <script>
    $(document).ready(function () {
      if ($('html').hasClass('desktop')) {
        new WOW().init();
      }
    });
  </script>
  <!--<![endif]-->
  <script>
    $(document).ready(function () {
      $('#camera_wrap').camera({
        loader: true,
        pagination: true,
        minHeight: '',
        thumbnails: false,
        height: '52.8735632183908%',
        caption: true,
        navigation: false,
        fx: 'simpleFade',
        onLoaded: function () {
          $('.slider-wrapper')[0].style.height = 'auto';
        }
      });
      $("#tabs").tabs();
      $('#bookingForm').bookingForm({
        ownerEmail: '#'
      });
      $('#bookingForm2').bookingForm({
        ownerEmail: '#'
      });
    });
  </script>
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <div id="ie6-alert" style="width: 100%; text-align:center;">
    <img src="http://beatie6.frontcube.com/images/ie6.jpg" alt="Upgrade IE 6" width="640" height="344" border="0" usemap="#Map" longdesc="http://die6.frontcube.com" />
    <map name="Map" id="Map"><area shape="rect" coords="496,201,604,329" href="http://www.microsoft.com/windows/internet-explorer/default.aspx" target="_blank" alt="Download Interent Explorer" /><area shape="rect" coords="380,201,488,329" href="http://www.apple.com/safari/download/" target="_blank" alt="Download Apple Safari" /><area shape="rect" coords="268,202,376,330" href="http://www.opera.com/download/" target="_blank" alt="Download Opera" /><area shape="rect" coords="155,202,263,330" href="http://www.mozilla.com/" target="_blank" alt="Download Firefox" />
      <area shape="rect" coords="35,201,143,329" href="http://www.google.com/chrome" target="_blank" alt="Download Google Chrome" />
    </map>
  </div>
  <![endif]-->
  <script src="js/jquery.tabs.js"></script>
</head>


<body>
<?$APPLICATION->ShowPanel()?>

<?if($USER->IsAdmin()):?>

<div style="border:red solid 1px">
	<form action="/bitrix/admin/site_edit.php" method="GET">
		<input type="hidden" name="LID" value="<?=SITE_ID?>" />
		<p><font style="color:red"><?echo GetMessage("DEF_TEMPLATE_NF")?> </font></p>
		<input type="submit" name="set_template" value="<?echo GetMessage("DEF_TEMPLATE_NF_SET")?>" />
	</form>
</div>

<?endif?>
<!--========================================================
                          HEADER
=========================================================-->
<header id="header">
  <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
    <div class="width-wrapper">
      <h1>
        <a href="index.html">
          <span class="wrapper"><i class="fa fa-car"></i><strong>MSK</strong>Motors</span>
        </a>
      </h1>
      <div class="authorization-block">
        <div class="authorization">
          <a class="create" href="#">Create an account</a>
          <span class="divider"></span>
          <a class="login" href="#">Login</a>
        </div>
        <a class="add btn-big" href="#"><span class="plus">+</span>Add advertisement</a>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <div id="stuck_container">
    <div class="width-wrapper">
      <nav>
        <ul class="sf-menu">
          <li class="current"><a href="index.html">Автосалон</a></li>
          <li><a href="index-1.html">Подержаные авто</a></li>
          <li><a href="index-2.html">Сервис</a>
            <ul>
              <li><a href="#">Lorem ipsum</a></li>
              <li><a href="#">Conse ctetur</a>
                <ul>
                  <li><a href="#">Pellentesque</a></li>
                  <li><a href="#">Aliquam congue</a></li>
                  <li><a href="#">Mauris accum</a></li>
                  <li><a href="#">Mctetur</a></li>
                </ul>
              </li>
              <li><a href="#">Do eiusmod</a></li>
              <li><a href="#">Incididunt ut</a></li>
              <li><a href="#">Et dolore</a></li>
              <li><a href="#">Ut enim ad minim</a></li>
            </ul>
	  </li>
          <li><a href="index-3.html">Кредитование</a></li>
          <li><a href="index-3.html">Страхование</a></li>
          <li><a href="index-4.html">Контакты</a></li>
        </ul>
      </nav>
      <form id="search" action="search.php" method="GET" accept-charset="utf-8">
        <input type="text" name="s"/>
        <a onclick="document.getElementById('search').submit()">
          <div class="search_icon"></div>
        </a>
      </form>
      <div class="clearfix"></div>
    </div>
  </div>
</header>


<!--========================================================
                          CONTENT
=========================================================-->
<section id="content">
  <div class="width-wrapper width-wrapper__inset1">
    <div class="wrapper1">
      <div class="container">
        <div class="row">
          <div class="grid_3">
            <div id="tabs">
              <div class="row">
                <div class="grid_3">
                  <ul class="tabs-list">
                    <li>
                      <a href="#tabs-1">
                        <div class="tab tab-first maxheight">
                          Used cars
                        </div>
                      </a>
                    </li>
                    <li>
                      <a href="#tabs-2">
                        <div class="tab maxheight">
                          New cars
                        </div>
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div id="tabs-1">
                <form id="bookingForm" class="bookingForm1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                  <span class="heading">Make:</span>
                  <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                    <option>Any Make</option>
                    <option>Any Make 1</option>
                    <option>Any Make 2</option>
                    <option>Any Make 3</option>
                    <option>Any Make 4</option>
                    <option>Any Make 5</option>
                  </select>
                  <span class="heading">Model:</span>
                  <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                    <option>Any Model</option>
                    <option>Any Model 1</option>
                    <option>Any Model 2</option>
                    <option>Any Model 3</option>
                    <option>Any Model 4</option>
                    <option>Any Model 5</option>
                  </select>
                  <span class="heading">Region:</span>
                  <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                    <option>Any</option>
                    <option>Any 1</option>
                    <option>Any 2</option>
                    <option>Any 3</option>
                    <option>Any 4</option>
                    <option>Any 5</option>
                  </select>
                  <div class="narrow-selects">
                    <div class="block-left">
                      <span class="heading">Min Year:</span>
                      <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                        <option>Min</option>
                        <option>Min 1</option>
                        <option>Min 2</option>
                        <option>Min 3</option>
                        <option>Min 4</option>
                        <option>Min 5</option>
                      </select>
                    </div>
                    <div class="block-right">
                      <span class="heading">Max Year:</span>
                      <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                        <option>2014</option>
                        <option>2013</option>
                        <option>2012</option>
                        <option>2011</option>
                        <option>2010</option>
                        <option>2009</option>
                      </select>
                    </div>

                    <div class="block-left">
                      <span class="heading">Min Price:</span>
                      <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                        <option>Min</option>
                        <option>Min 1</option>
                        <option>Min 2</option>
                        <option>Min 3</option>
                        <option>Min 4</option>
                        <option>Min 5</option>
                      </select>
                    </div>
                    <div class="block-right">
                      <span class="heading">Max Price:</span>
                      <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                        <option>Max</option>
                        <option>Max 1</option>
                        <option>Max 2</option>
                        <option>Max 3</option>
                        <option>Max 4</option>
                        <option>Max 5</option>
                      </select>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <span class="heading">Body Type:</span>
                  <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                    <option>Any Make</option>
                    <option>Any Make 1</option>
                    <option>Any Make 2</option>
                    <option>Any Make 3</option>
                    <option>Any Make 4</option>
                    <option>Any Make 5</option>
                  </select>
                  <a href="#" class="btn-big" data-type="submit">Find a car</a>
                  <a href="#" class="simple">Detailed Search</a>
                </form>
              </div>
              <div id="tabs-2">
                <form id="bookingForm2" class="bookingForm1">
                  <span class="heading">Make:</span>
                  <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                    <option>Any Make</option>
                    <option>Any Make 1</option>
                    <option>Any Make 2</option>
                    <option>Any Make 3</option>
                    <option>Any Make 4</option>
                    <option>Any Make 5</option>
                  </select>
                  <span class="heading">Model:</span>
                  <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                    <option>Any Model</option>
                    <option>Any Model 1</option>
                    <option>Any Model 2</option>
                    <option>Any Model 3</option>
                    <option>Any Model 4</option>
                    <option>Any Model 5</option>
                  </select>
                  <span class="heading">Region:</span>
                  <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                    <option>Any</option>
                    <option>Any 1</option>
                    <option>Any 2</option>
                    <option>Any 3</option>
                    <option>Any 4</option>
                    <option>Any 5</option>
                  </select>
                  <div class="narrow-selects">
                    <div class="block-left">
                      <span class="heading">Min Year:</span>
                      <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                        <option>Min</option>
                        <option>Min 1</option>
                        <option>Min 2</option>
                        <option>Min 3</option>
                        <option>Min 4</option>
                        <option>Min 5</option>
                      </select>
                    </div>
                    <div class="block-right">
                      <span class="heading">Max Year:</span>
                      <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                        <option>2014</option>
                        <option>2013</option>
                        <option>2012</option>
                        <option>2011</option>
                        <option>2010</option>
                        <option>2009</option>
                      </select>
                    </div>

                    <div class="block-left">
                      <span class="heading">Min Price:</span>
                      <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                        <option>Min</option>
                        <option>Min 1</option>
                        <option>Min 2</option>
                        <option>Min 3</option>
                        <option>Min 4</option>
                        <option>Min 5</option>
                      </select>
                    </div>
                    <div class="block-right">
                      <span class="heading">Max Price:</span>
                      <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                        <option>Max</option>
                        <option>Max 1</option>
                        <option>Max 2</option>
                        <option>Max 3</option>
                        <option>Max 4</option>
                        <option>Max 5</option>
                      </select>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                  <span class="heading">Body Type:</span>
                  <select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
                    <option>Any Make</option>
                    <option>Any Make 1</option>
                    <option>Any Make 2</option>
                    <option>Any Make 3</option>
                    <option>Any Make 4</option>
                    <option>Any Make 5</option>
                  </select>
                  <a href="#" class="btn-big" data-type="submit">Find a car</a>
                  <a href="#" class="simple">Detailed Search</a>
                </form>
              </div>
            </div>
            <div class="banner1">
              <h2 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                <a href="#">About our Services</a>
              </h2>
              <p class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit.</p>
            </div>
            <div class="post1">
              <h2 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">Latest REVIEWS</h2>
              <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img1.png" alt=""/>
              <time class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s" datetime="2014-01-01">Monday, June 24, 2013</time>
              <h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s">
                <a href="#">Lorem ipsum dolor sit amet conse</a>
              </h3>
              <p class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">Lorem ipsum dolor sit amet conse ctetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip.</p>
            </div>
          </div>
          <div class="grid_9">
            <div class="slider-wrapper">
              <div id="camera_wrap">
                <div data-src="images/slider1.png">
                  <div class="caption fadeIn">
                    <div class="heading">
                      <span class="first">2014 Mercedes-Benz</span>
                      <span class="second">GLK-Class <span class="price">$89999</span></span>
                    </div>
                    <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                      <span class="first">
                        Mileage: <strong>130 000</strong> km
                      </span>
                      <span class="second">
                        Volume capacity: <strong>1.3</strong>l
                      </span>
                    </div>
                  </div>
                </div>
                <div data-src="images/slider2.png">
                  <div class="caption fadeIn">
                    <div class="heading">
                      <span class="first">2014 Lexus</span>
                      <span class="second">GX 460 <span class="price">$89999</span></span>
                    </div>
                    <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                      <span class="first">
                        Mileage: <strong>130 000</strong> km
                      </span>
                      <span class="second">
                        Volume capacity: <strong>1.3</strong>l
                      </span>
                    </div>
                  </div>
                </div>
                <div data-src="images/slider3.png">
                  <div class="caption fadeIn">
                    <div class="heading">
                      <span class="first">2014 Lexus</span>
                      <span class="second">LF-NX Hybrid <span class="price">$89999</span></span>
                    </div>
                    <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                      <span class="first">
                        Mileage: <strong>130 000</strong> km
                      </span>
                      <span class="second">
                        Volume capacity: <strong>1.3</strong>l
                      </span>
                    </div>
                  </div>
                </div>
                <div data-src="images/slider4.png">
                  <div class="caption fadeIn">
                    <div class="heading">
                      <span class="first">2014 Cadillac</span>
                      <span class="second">Escalade <span class="price">$149999</span></span>
                    </div>
                    <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                      <span class="first">
                        Mileage: <strong>130 000</strong> km
                      </span>
                      <span class="second">
                        Volume capacity: <strong>1.3</strong>l
                      </span>
                    </div>
                  </div>
                </div>
                <div data-src="images/slider5.png">
                  <div class="caption fadeIn">
                    <div class="heading">
                      <span class="first">2014 Lexus</span>
                      <span class="second">IS 350 F Sport <span class="price">$49999</span></span>
                    </div>
                    <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                      <span class="first">
                        Mileage: <strong>130 000</strong> km
                      </span>
                      <span class="second">
                        Volume capacity: <strong>1.3</strong>l
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="wrapper2">
              <div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                <h2>Latest used cars</h2>
              </div>
              <div class="border-wrapper1 wrapper3">
                <div class="row">
                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img2.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img3.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img4.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="border-wrapper1 wrapper3">
                <div class="row">
                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img5.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img6.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img7.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="border-wrapper1 wrapper3">
                <div class="row">
                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img8.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img9.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img10.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="border-wrapper1 wrapper3">
                <div class="row">
                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img11.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img12.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>

                  <div class="grid_3">
                    <div class="box1">
                      <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                        <a href="#">2011  Lorem ipsum dolor</a>
                      </h4>
                      <img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="images/page1_img13.png" alt=""/>
                      <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                        <span class="first">Mileage: <span class="highlighted">130 000 km</span></span>
                        <span class="second">Volume capacity: <span class="highlighted">1.3l</span></span>
                        <div class="clearfix"></div>
                      </div>
                      <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                        <div class="price">
                          <span class="first">Price:</span>
                          <span class="second">$26899</span>
                        </div>
                        <a class="btn-default" href="#">
                          <span>Details</span>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            <div class="button-wrapper1">
              <a class="btn-big-green wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s" href="#">
                <span>See all cars</span>
              </a>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

