<?php
/* @class CBitrixComponentTemplate */
/**
0 => string 'arResult' (length=8)
1 => string 'arParams' (length=8)
2 => string 'parentTemplateFolder' (length=20)
3 => string 'APPLICATION' (length=11)
4 => string 'USER' (length=4)
5 => string 'DB' (length=2)
6 => string 'templateName' (length=12)
7 => string 'templateFile' (length=12)
8 => string 'templateFolder' (length=14)
9 => string 'componentPath' (length=13)
10 => string 'component' (length=9)
11 => string 'templateData' (length=12)
 **/


?>
<div>
    <h3>Похожие автомобили</h3>
</div>
<div class="border-wrapper1 wrapper3">
    <div class="row">

        <?php if ($arResult['VEHICLES']) : ?>

        <? $iautos=0 ?>

        <? foreach ($arResult['VEHICLES'] as $position => $arVehicle) : ?>
        <div class="grid_3">
            <div class="box1">
                <h4 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                    <a href="<?= $arVehicle['URL_TO_VEHICLE']?>"><?= $arVehicle['YEAR'] ?> <?= $arVehicle['MARK']['NAME'] ?> <?= ((18 < mb_strlen($arVehicle['MODEL']['NAME']) && $pos = mb_strpos($arVehicle['MODEL']['NAME'], '(', 5)) ? trim(mb_substr($arVehicle['MODEL']['NAME'], 0, $pos)) : $arVehicle['MODEL']['NAME']) ?></a>
                </h4>
                <? if ($arVehicle['PHOTO']) : ?>
                    <a href="<?= $arVehicle['URL_TO_VEHICLE']?>"><img class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s" src="<?=$arVehicle['PHOTO']['0']['MED']['1'];?>"  alt="<?= htmlspecialchars($arVehicle['MARK']['NAME']), ' ', htmlspecialchars($arVehicle['MODEL_NAME']);?>" width="270" height="202" /></a>
                <? else : ?>
                    <a href="<?= $arVehicle['URL_TO_VEHICLE']?>"><img src="/bitrix/images/maxposter/no-image-tn.png" width="270" height="202" /></a>
                <? endif ?>
                <div class="info wow fadeIn" data-wow-duration="1s" data-wow-delay=".2s">
                    <span class="first">Пробег <span class="highlighted"><?= number_format($arVehicle['DISTANCE'], '0', '.', ' '), ' ', ('km' == $arVehicle['DISTANCE_UNIT'] ? 'км' : 'миль'); ?></span></span>
                    <span class="second">Двигатель <span class="highlighted"><?= $arVehicle['ENGINE_VOLUME'] ?>&nbsp;см&#179;</span></span>
                    <div class="clearfix"></div>
                </div>
                <div class="info2 wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                    <div class="price">
                        <span class="first">Цена</span>
                        <span class="second"><?= maxPrice($arVehicle['PRICE'], $arVehicle['PRICE_UNIT']) ?></span>
                    </div>
                    <a class="btn-default" href="<?= $arVehicle['URL_TO_VEHICLE']?>">
                        <span>Подробнее</span>
                    </a>
                    <div class="clearfix"></div>
                </div>
            </div>

        </div>
        <? if (++$iautos%4==0)	:?>
    </div>
</div>
<div class="border-wrapper1 wrapper3">
    <div class="row">
        <? endif ?>
        <?php endforeach ?>


        <?php if ($arResult['PAGER'] && (1 < $arResult['PAGER']['TOTAL'])) : ?>
            <div class="grid_9">
                <div class="box1">
                    <div class="page_navigation">
                        <?php if (1 < $arResult['PAGER']['CURRENT']) : ?>
                            <a href="<?php echo $arResult['PAGER']['PAGES']['NUM'][$arResult['PAGER']['CURRENT']-1] ?>" class="pn_prev">&nbsp;</a>
                        <?php endif ?>
                        <?php foreach ($arResult['PAGER']['PAGES']['NUM'] as $num => $url) : ?>
                            <?php if ($arResult['PAGER']['CURRENT'] == $num) : ?>
                                <a href="javascript:void(0)" class="pn_page_akt">&nbsp;<?php echo $num ?>&nbsp;</a>
                            <?php else : ?>
                                <a href="<?php echo $url ?>" class="pn_page">&nbsp;<?php echo $num ?>&nbsp;</a>
                            <?php endif ?>
                        <?php endforeach ?>
                        <?php if ($arResult['PAGER']['TOTAL'] > $arResult['PAGER']['CURRENT']) : ?>
                            <a href="<?php echo $arResult['PAGER']['PAGES']['NUM'][$arResult['PAGER']['CURRENT']+1] ?>" class="pn_next">&nbsp;</a>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <?php endif ?>
    </div>
</div>

