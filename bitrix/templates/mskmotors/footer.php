<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?>                </div>
<? if (CSite::InDir(SITE_DIR . "index.php")) {
    ?>

    <?
} elseif (CSite::InDir(SITE_DIR . "uslugi/kreditovanie/index.php") || CSite::InDir(SITE_DIR . "uslugi/strahovanie/index.php")) {
    ?>

    <?
    $APPLICATION->IncludeComponent("bitrix:main.include", "", array(
        "COMPONENT_TEMPLATE" => ".default",
        "AREA_FILE_SHOW" => "page",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => ""
    ),
        false,
        array(
            "ACTIVE_COMPONENT" => "Y"
        )
    ); ?><?
} else {

}
?>

<style>
    .calctext {
        border: 0;
        color: #0094D7;
        background-color: #F5F5F5;
        font-weight: bold;
        font-size: 20px;
        font-family: Arial;
    }

    .thint {
        border: 0;
        width: 135px;
        color: #BCBCBC;
        background-color: #F5F5F5;
        font-size: 18px;
        font-family: Arial;
    }

    .calctd td {
        font-family: Arial;
        font-size: 16px;
    }
</style>

<div id="calculator" style="width:470px; display: none; height:766px;border:1px;border-color:#666666;background-color:#F5F5F5;padding:10px;border-radius: 6px;">
    <form id="calc" style="width: 433px">
        <table width=450px cellpadding=3 class=calctd>
            <tr>
                <td align=center style="padding-bottom:10px;"><b>Кредитный калькулятор</b></td>
            </tr>
            <tr>
                <td align=center style="padding-bottom:10px;">
                    <div>
                        <div class="image-credit">
                            <img class="img-credit" src="<?= $image_credit ?>" alt="">
                        </div>
                        <div class="description-credit">
                            <p id="name"><?= $mark_name; ?><?= $model_name; ?></p>
                            <p id="desc"><?= $year; ?> г., <?= $distance; ?> км, <?= $engine_volume; ?> <?= $engine_type; ?>, <?= $gearbox_type; ?></p>
                            <input type="hidden" id="sum" name="sum" value="<? echo $price_dolcar; ?> Р"/>
                            <input type="text" name="showsum" value="<? echo $price_dolcar; ?>" class="calctext" style="width:83px; float: left" readonly>
                            <p style="padding: 5px">р.</p>
                        </div>
                    </div>
                </td>
            </tr>

            <tr>
                <td>

                </td>
            </tr>
            <tr>
                <td>
                    <label style="width: 100%;" for="initial">Первоначальный взнос:</label>
                    <input type="text" id="initial" name="initial" value="10000"/>
                    <input type="hidden" name="initialrange" value="10000" class="calctext" style="width:100px;" readonly>
                    <div id="initialrange"></div>
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>


            <tr style="display: none">
                <td>
                    <div id="sumrange"></div>
                </td>
            </tr>

            <br>

            <tr>
                <td valign=middle><label style="width: 100%; margin-top: 6px;" for="time">Срок (месяцев):</label>
                    <input type="text" id="time" name="time" value="12"/>
                    <input type="hidden" name="timehint" id="timehint" value="6" class="thint1" readonly>
                    <div id="timerange"></div>
                </td>
            </tr>
            <tr>
                <td>Ставка: <input type=text name=stavka value=15 class=calctext style="width:27px;" readonly>%
                </td>
            </tr>

            <tr>
                <td>В месяц: <input type=text name=res class=calctext style="width:100px;" value="16 972" readonly>
                </td>
            </tr>
            <tr>
                <td>Сумма кредита: <input type=text name=summcredit class=calctext style="width:100px;" value="120 000" readonly>
                </td>
            </tr>
            <div>
            </div>
    </form>
    <? $APPLICATION->IncludeComponent(
        "bitrix:form.result.new",
        "form_credit",
        array(
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "CHAIN_ITEM_LINK" => "",
            "CHAIN_ITEM_TEXT" => "",
            "COMPONENT_TEMPLATE" => "form_credit",
            "EDIT_URL" => "",
            "IGNORE_CUSTOM_TEMPLATE" => "Y",
            "LIST_URL" => "",
            "SEF_MODE" => "N",
            "SUCCESS_URL" => "/",
            "USE_EXTENDED_ERRORS" => "Y",
            "WEB_FORM_ID" => "10",
            "VARIABLE_ALIASES" => array(
                "WEB_FORM_ID" => "WEB_FORM_ID",
                "RESULT_ID" => "RESULT_ID",
            )
        ),
        false
    ); ?>

    </table>


</div>


<div id="calculator_list" style="width:470px; display: none; height:413px;border:1px;border-color:#666666;background-color:#F5F5F5;padding:10px;border-radius: 6px;">
    <div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:form.result.new",
            "form_credit",
            array(
                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "A",
                "CHAIN_ITEM_LINK" => "",
                "CHAIN_ITEM_TEXT" => "",
                "COMPONENT_TEMPLATE" => "form_credit",
                "EDIT_URL" => "",
                "IGNORE_CUSTOM_TEMPLATE" => "Y",
                "LIST_URL" => "",
                "SEF_MODE" => "N",
                "SUCCESS_URL" => "_",
                "USE_EXTENDED_ERRORS" => "Y",
                "WEB_FORM_ID" => "10",
                "VARIABLE_ALIASES" => array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID",
                )
            ),
            false
        ); ?>
    </div>

</div>


<div id="service_order" style="width:470px; display: none; height:413px;border:1px;border-color:#666666;background-color:#F5F5F5;padding:10px;border-radius: 6px;">
    <div>
        <? $APPLICATION->IncludeComponent(
            "bitrix:form.result.new",
            "form_service",
            array(
                "CACHE_TIME" => "3600",
                "CACHE_TYPE" => "A",
                "CHAIN_ITEM_LINK" => "",
                "CHAIN_ITEM_TEXT" => "",
                "COMPONENT_TEMPLATE" => "form_service",
                "EDIT_URL" => "",
                "IGNORE_CUSTOM_TEMPLATE" => "Y",
                "LIST_URL" => "",
                "SEF_MODE" => "N",
                "SUCCESS_URL" => "/",
                "USE_EXTENDED_ERRORS" => "Y",
                "WEB_FORM_ID" => "13",
                "VARIABLE_ALIASES" => array(
                    "WEB_FORM_ID" => "WEB_FORM_ID",
                    "RESULT_ID" => "RESULT_ID",
                )
            ),
            false
        ); ?>
    </div>

</div>


<div id="trade" style="width:475px; display: none; height:493px;border:1px;border-color:#666666;background-color:#F5F5F5;padding:10px;border-radius: 6px;">

    <table width=450px cellpadding=3 class=calctd>
        <tr>
            <td align=center style="padding-bottom:30px;"><b>Хочу обменять свой автомобиль на этот</b></td>
        </tr>
        <tr>
            <td align=center style="padding-bottom:10px;">
                <div>
                    <div class="image-credit">
                        <img class="img-credit" src="<?= $image_credit ?>" alt="">
                    </div>
                    <div class="description-credit">
                        <p id="name"><?= $mark_name; ?><?= $model_name; ?></p>
                        <p id="desc"><?= $year; ?> г., <?= $distance; ?> км, <?= $engine_volume; ?> <?= $engine_type; ?>, <?= $gearbox_type; ?></p>
                        <input type="hidden" id="sum" name="sum" value="<? echo $price_dolcar; ?> Р"/>
                        <input type="text" name="showsum" value="<? echo $price_dolcar; ?>" class="calctext" style="width:70px; float: left" readonly>
                        <p style="padding: 5px">р.</p>
                    </div>

                </div>
            </td>
        </tr>
    </table>
    <? $APPLICATION->IncludeComponent(
        "bitrix:form.result.new",
        "auto_trade",
        Array(
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "CHAIN_ITEM_LINK" => "",
            "CHAIN_ITEM_TEXT" => "",
            "COMPONENT_TEMPLATE" => ".default",
            "EDIT_URL" => "result_edit.php",
            "IGNORE_CUSTOM_TEMPLATE" => "N",
            "LIST_URL" => "result_list.php",
            "SEF_MODE" => "N",
            "SUCCESS_URL" => "",
            "USE_EXTENDED_ERRORS" => "N",
            "VARIABLE_ALIASES" => Array("RESULT_ID" => "RESULT_ID", "WEB_FORM_ID" => "WEB_FORM_ID"),
            "WEB_FORM_ID" => "8"
        )
    ); ?>
</div>


</div>
</div>
</div>
</div>
</section>
<!--========================================================
                          FOOTER
=========================================================-->
<footer id="footer">
    <div class="container">
        <div class="width-wrapper width-wrapper__inset1 width-wrapper__inset2">
            <div class="wrapper4">
                <div class="container-margin">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="box2">
                                <div class="box2">
                                    <h5><span>Меню сайта</span></h5>
                                    <ul class="list1">
                                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="/buy/">Купить автомобиль</a></li>

                                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="/avtomobili-s-probegom/">Автомобили в продаже</a></li>

                                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="/buy/">Подбор автомобиля</a></li>

                                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s"><a href="/sale/">Продать автомобиль</a></li>

                                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s"><a href="/service/">Сервис</a></li>

                                        <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s"><a href="/contacts/">Контакты</a></li>


                                    </ul>
                                </div>
                            </div>

                            <div class="share-top hidden-xs">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row row-share">
                                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                                <a href="http://www.drive2.ru/users/dolcar/" target="_blank" class="share-link">
                                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/apple-touch-icon.png" width="28px" height="26px">
                                                </a>
                                                <a href="https://instagram.com/dol.car/" target="_blank" class="share-link">
                                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram-iconv2.png" width="28px" height="26px">
                                                </a>
                                                <a href="https://www.youtube.com/channel/UCJEHM6UjMIl3dJkicymeleA" target="_blank" class="share-link">
                                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/youtube.png" width="28px" height="26px">
                                                </a>
                                                <a href="https://www.facebook.com/dolcar.su/" target="_blank" class="share-link">
                                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/fb.png" width="28px" height="26px">
                                                </a>
                                                <a href="http://vk.com/club21891706" target="_blank" class="share-link">
                                                    <img src="<?= SITE_TEMPLATE_PATH ?>/images/vkontakte-logo.svg" width="28px" height="26px">
                                                </a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-3">
                            <div class="box2">
                                <h5><span>Услуги</span></h5>
                                <ul class="list1">
                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="/credit/">Кредитование</a></li>
                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="/service/friends/">Сервис для друзей</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="/uslugi/strahovanie/">Страхование автомобиля</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="/uslugi/oformlenie/">Оформление документов</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="/uslugi/oformlenie/" class="selected">Помощь в регистрационных действиях</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="box2">
                                <h5><span>Сервис</span></h5>
                                <ul class="list1">
                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="/service/">Сервисное обслуживание</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="http://dolcar.su/">Установка доп.оборудования</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="/service/remont-avtoelektroniki/">Услуги автоэлектроника</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="/service/zapchasti/" class="selected">Подбор запчастей</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="box2">
                                <h5><span>О нас</span></h5>
                                <ul class="list1">
                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="/about/">О компании</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="/otzyvy/">Отзывы</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="/articles/">Статьи</a></li>

                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="/guarantees/" class="selected">Гарантии</a></li>
                                    <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="/contacts/" class="selected">Контакты, схема проезда, часы работы</a></li>
                                </ul>
                            </div>
                            <p style="color: white">Ежедневно с 9:00 до 21:00</br> МКАД Север, 79-й км,
                                г.Долгопрудный,
                                Транспортный проезд, д.3
                                (Напротив здания ГИБДД)
                                +7-495-25-500-25
                                sales@dolcar.ru</p>
                        </div>

                    </div>
                    <div class="share-top hidden-sm hidden-md hidden-lg">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="row row-share">
                                    <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                        <a href="http://www.drive2.ru/users/dolcar/" target="_blank" class="share-link">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/apple-touch-icon.png" width="28px" height="26px">
                                        </a>
                                        <a href="https://instagram.com/dol.car/" target="_blank" class="share-link">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/instagram-iconv2.png" width="28px" height="26px">
                                        </a>
                                        <a href="https://www.youtube.com/channel/UCJEHM6UjMIl3dJkicymeleA" target="_blank" class="share-link">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/youtube.png" width="28px" height="26px">
                                        </a>
                                        <a href="https://www.facebook.com/dolcar.su/" target="_blank" class="share-link">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/fb.png" width="28px" height="26px">
                                        </a>
                                        <a href="http://vk.com/club21891706" target="_blank" class="share-link">
                                            <img src="<?= SITE_TEMPLATE_PATH ?>/images/vkontakte-logo.svg" width="28px" height="26px">
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-margin">
                <div class="privacy-block wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
                    <? $APPLICATION->IncludeFile(
                        "/include/copyright.php",
                        Array(),
                        Array("MODE" => "php", "NAME" => "копирайт сайта")
                    ); ?>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<? $APPLICATION->IncludeFile(
    "/include/counter.php",
    Array(),
    Array("MODE" => "php", "NAME" => "счетчик сайта")
); ?>
<!-- ВЫВОД ВСПЛЫВАЮЩИХ ОКОН -->

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/templates/mskmotors/onvt_na_formy.php") ?>

<div class="wr_time_area" style="display: none;">

    <? $ii = 1; ?>
    <? if ($arResult["OTVET_FORM"]) { ?>
        <? foreach ($arResult["OTVET_FORM"] as $value) { ?>

            <div id="time_area_<?= $ii; ?>" class="time_area" style="display: none;">
                <? /* <h2><?=$value['NAME']; ?></h2>  */ ?>
                <div class="time_tx"><?= $value['PREVIEW_TEXT']; ?></div>
            </div>

            <? $ii++; ?>
        <? } ?>
    <? } ?>
</div>  <!----  wr_time_area  ---->

<!-- ВЫВОД ВСПЛЫВАЮЩИХ ОКОН -->
<script>
    $(document).ready(function () {

        $(".callback, .add, .credit, .question").fancybox();


        var otz = $(".otp_otz").data("otz");
        console.log(otz);
        if (otz == 10) {
            var st_adr = "/buy";
            window.history.replaceState(null, null, st_adr.toString());

            $.fancybox.open("#time_area_1");
            setTimeout("$.fancybox.close()", 3200);
            setTimeout("location.reload()", 3400);
        }

        if (otz == 20) {
            var st_adr = "_";
            window.history.replaceState(null, null, st_adr.toString());

            $.fancybox.open("#time_area_2");
            setTimeout("$.fancybox.close()", 3200);
            setTimeout("location.reload()", 3400);
        }

        if (otz == 30) {
            var st_adr = "/";
            window.history.replaceState(null, null, st_adr.toString());

            $.fancybox.open("#time_area_1");
            setTimeout("$.fancybox.close()", 3200);
            setTimeout("location.reload()", 3400);
        }

        if (otz == 40) {
            var st_adr = "_";
            window.history.replaceState(null, null, st_adr.toString());

            $.fancybox.open("#time_area_4");
            setTimeout("$.fancybox.close()", 3200);
            setTimeout("location.reload()", 3400);
        }

//------------
    });
</script>

<script>
    //$("#form_tel [name=\"web_form_submit\"]").on('click', function(e){
    //	e.preventDefault();
    //	alert($("#form_tel [name=\"form_text_1\"]").val());

    //});

</script>
<? $APPLICATION->IncludeComponent(
    "bitrix:main.include",
    "",
    Array(
        "AREA_FILE_SHOW" => "file",
        "AREA_FILE_SUFFIX" => "inc",
        "EDIT_TEMPLATE" => "",
        "PATH" => SITE_TEMPLATE_PATH . "/include_areas/pformi.php"
    )
); ?>

<!-- StreamWood code -->
<link href="https://clients.streamwood.ru/StreamWood/sw.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="https://clients.streamwood.ru/StreamWood/sw.js" charset="utf-8"></script>
<script type="text/javascript">
    swQ(document).ready(function () {
        swQ().SW({
            swKey: 'ebac6fa71114b9d46151432f1c2bd190',
            swDomainKey: '950f74609a3b9680cf05e5a7b5665054'
        });
        swQ('body').SW('load');
    });
</script>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/slideout.min.js"></script>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/cars.js"></script>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/mark-model.js"></script>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.validate.js"></script>

<script src="<?= SITE_TEMPLATE_PATH ?>/js/messages_ru.js"></script>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css"/>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/nouislider.min.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/scripts.js"></script>
<script src="<?= SITE_TEMPLATE_PATH ?>/js/bootstrap.js"></script>


<!-- /StreamWood code -->


<div id="form_question" style="display: none">
    <? $APPLICATION->IncludeComponent(
        "bitrix:form.result.new",
        "form_question",
        array(
            "CACHE_TIME" => "3600",
            "CACHE_TYPE" => "A",
            "CHAIN_ITEM_LINK" => "",
            "CHAIN_ITEM_TEXT" => "",
            "COMPONENT_TEMPLATE" => "form_question",
            "EDIT_URL" => "result_edit.php",
            "IGNORE_CUSTOM_TEMPLATE" => "Y",
            "LIST_URL" => "result_list.php",
            "SEF_MODE" => "N",
            "SUCCESS_URL" => "/",
            "USE_EXTENDED_ERRORS" => "Y",
            "WEB_FORM_ID" => "4",
            "VARIABLE_ALIASES" => array(
                "WEB_FORM_ID" => "WEB_FORM_ID",
                "RESULT_ID" => "RESULT_ID",
            )
        ),
        false
    ); ?>
</div>
<div id="form_tel" style="display: none;">
    <? $APPLICATION->IncludeComponent(
        "bitrix:form.result.new",
        "form_tel",
        array(
            "SEF_MODE" => "N",
            "WEB_FORM_ID" => "1",
            "LIST_URL" => "",
            "EDIT_URL" => "",
            "SUCCESS_URL" => "",
            "CHAIN_ITEM_TEXT" => "",
            "CHAIN_ITEM_LINK" => "",
            "IGNORE_CUSTOM_TEMPLATE" => "N",
            "USE_EXTENDED_ERRORS" => "N",
            "CACHE_TYPE" => "A",
            "CACHE_TIME" => "3600",
            "COMPONENT_TEMPLATE" => "form_tel",
            "VARIABLE_ALIASES" => array(
                "WEB_FORM_ID" => "WEB_FORM_ID",
                "RESULT_ID" => "RESULT_ID",
            )
        ),
        false
    ); ?>
</div>

</body>
</html>