<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
    die();
?><!DOCTYPE html>
<head>
    <?$APPLICATION->ShowHead();?>
    <title><?$APPLICATION->ShowTitle();?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="yandex-verification" content="e60d9f6cc25340e3" />
    <meta name="cmsmagazine" content="9be3e4d2e3183ca613e258a224d10d72" />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/grid.css");?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/animate.css");?>
    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/nouislider.min.css");?>

    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/custom.css");?>


    <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/normalize.css");?>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700|Roboto+Condensed:400,700,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>  <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.fancybox.css">

    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/bootstrap.css">
    <style> .sf-menu {
            margin-top: 0 !important;
            margin-bottom: 0px !important;
        }
        .grid_3 {
            width: 261px !important;
        }

        .sf-menu ul {
            /* display: none; */
            top: 58px !important;
            z-index: 10 !important;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }

        #sumrange {
            width: 253px;
        }

        #timerange {
            width: 253px;
        }

        #header #stuck_container:after {
            background: white;
            width: 100%;
            height: 31px;
            position: absolute;
            content: '';
            bottom: -31px;
            left: 0;
        }

    </style>

    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vuex/3.0.1/vuex.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.2/css/bulma.min.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.2.1.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/jquery.fancybox.pack.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/classie.js"></script>
    <script src="<?=SITE_TEMPLATE_PATH?>/js/selectFx.js"></script>


    <script src="<?=SITE_TEMPLATE_PATH?>/js/jssor.slider-24.1.5.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        jssor_1_slider_init = function() {

            var jssor_1_SlideoTransitions = [
                [{b:0,d:600,y:-290,e:{y:27}}],
                [{b:0,d:1000,y:185},{b:1000,d:500,o:-1},{b:1500,d:500,o:1},{b:2000,d:1500,r:360},{b:3500,d:1000,rX:30},{b:4500,d:500,rX:-30},{b:5000,d:1000,rY:30},{b:6000,d:500,rY:-30},{b:6500,d:500,sX:1},{b:7000,d:500,sX:-1},{b:7500,d:500,sY:1},{b:8000,d:500,sY:-1},{b:8500,d:500,kX:30},{b:9000,d:500,kX:-30},{b:9500,d:500,kY:30},{b:10000,d:500,kY:-30},{b:10500,d:500,c:{x:87.50,t:-87.50}},{b:11000,d:500,c:{x:-87.50,t:87.50}}],
                [{b:0,d:600,x:410,e:{x:27}}],
                [{b:-1,d:1,o:-1},{b:0,d:600,o:1,e:{o:5}}],
                [{b:-1,d:1,c:{x:175.0,t:-175.0}},{b:0,d:800,c:{x:-175.0,t:175.0},e:{c:{x:7,t:7}}}],
                [{b:-1,d:1,o:-1},{b:0,d:600,x:-570,o:1,e:{x:6}}],
                [{b:-1,d:1,o:-1,r:-180},{b:0,d:800,o:1,r:180,e:{r:7}}],
                [{b:0,d:1000,y:80,e:{y:24}},{b:1000,d:1100,x:570,y:170,o:-1,r:30,sX:9,sY:9,e:{x:2,y:6,r:1,sX:5,sY:5}}],
                [{b:2000,d:600,rY:30}],
                [{b:0,d:500,x:-105},{b:500,d:500,x:230},{b:1000,d:500,y:-120},{b:1500,d:500,x:-70,y:120},{b:2600,d:500,y:-80},{b:3100,d:900,y:160,e:{y:24}}],
                [{b:0,d:1000,o:-0.4,rX:2,rY:1},{b:1000,d:1000,rY:1},{b:2000,d:1000,rX:-1},{b:3000,d:1000,rY:-1},{b:4000,d:1000,o:0.4,rX:-1,rY:-1}]
            ];

            var jssor_1_options = {
                $AutoPlay: 1,
                $Idle: 6000,
                $CaptionSliderOptions: {
                    $Class: $JssorCaptionSlideo$,
                    $Transitions: jssor_1_SlideoTransitions,
                    $Breaks: [
                        [{d:2000,b:1000}]
                    ]
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$,
                    $SpacingX: 12,
                    $SpacingY: 6
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1180);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*responsive code end*/
        };
    </script>
    <style>
        /* jssor slider bullet navigator skin 01 css */
        /*
        .jssorb01 div           (normal)
        .jssorb01 div:hover     (normal mouseover)
        .jssorb01 .av           (active)
        .jssorb01 .av:hover     (active mouseover)
        .jssorb01 .dn           (mousedown)
        */
        .jssorb01 {
            position: absolute;
        }
        .jssorb01 div, .jssorb01 div:hover, .jssorb01 .av {
            position: absolute;
            /* size of bullet elment */
            width: 12px;
            height: 12px;
            filter: alpha(opacity=70);
            opacity: .7;
            overflow: hidden;
            cursor: pointer;
            border: #000 1px solid;
        }
        .jssorb01 div { background-color: gray; }
        .jssorb01 div:hover, .jssorb01 .av:hover { background-color: #d3d3d3; }
        .jssorb01 .av { background-color: #fff; }
        .jssorb01 .dn, .jssorb01 .dn:hover { background-color: #555555; }

        /* jssor slider arrow navigator skin 02 css */
        /*
        .jssora02l                  (normal)
        .jssora02r                  (normal)
        .jssora02l:hover            (normal mouseover)
        .jssora02r:hover            (normal mouseover)
        .jssora02l.jssora02ldn      (mousedown)
        .jssora02r.jssora02rdn      (mousedown)
        */
        .jssora02l, .jssora02r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 55px;
            height: 55px;
            cursor: pointer;
            background: url('img/a02.png') no-repeat;
            overflow: hidden;
        }
        .jssora02l { background-position: -3px -33px; }
        .jssora02r { background-position: -63px -33px; }
        .jssora02l:hover { background-position: -123px -33px; }
        .jssora02r:hover { background-position: -183px -33px; }
        .jssora02l.jssora02ldn { background-position: -3px -33px; }
        .jssora02r.jssora02rdn { background-position: -63px -33px; }
    </style>


</head>
<body>
<div id="panel">
    <?$APPLICATION->ShowPanel();?>
</div>
<!--========================================================
                          HEADER
=========================================================-->

<style>
    /*menu*/
    .header-top {
        background-color: #363636;
        padding-bottom:22px;
    }
    .image-resize {
        width:140px;
        height:64px;
    }


    .navbar.is-fixed-top {
        top: 0;
        padding-left: 20px;
        padding-right: 20px;
        /*padding-left: 330px !important;*/
        /*padding-right: 338px !important;*/
    }


    .navbar-item, .navbar-link{
        padding: .5rem .6rem;
    }
    .navbar-link {
        padding-right: 2em;
    }
    @media screen and (max-device-width: 1440px) {
        .navbar.is-fixed-top {
            top: 0;
            /*padding-left: 65px !important;*/
            /*padding-right: 338px !important;*/
        }
    }








    .is-fullhd {
        max-width:1265px !important;
    }
    .logo-item {
        max-height:4.75rem !important;
        max-width: 7rem !important;
    }
    .is-fixed-top {
        padding: 8px 0 10px 0;
    }
    .navbar {
        border-radius:0px !important;
    }
    /*menu*/

    /*filter*/
    .filter-top {
        background-color: #f06230;
        padding: 0px 4px 20px;
    }
    /*filter*/
</style>


<section class="section header-top">
    <div class="container is-fullhd">
    <nav class="navbar is-dark is-fixed-top">
            <div class="navbar-brand">
                <a class="navbar-item nav-logo" href="/">
                    <img src="<?=SITE_TEMPLATE_PATH?>/images/new-logo.png" class="logo-item" alt="Bulma: a modern CSS framework based on Flexbox" width="150" height="90">
                </a>
                <div class="navbar-burger burger" data-toggle="collapse" data-target="#navbarExampleTransparentExample">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>

            <div id="navbarExampleTransparentExample" class="navbar-menu">

                <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"dolcar_multilevel", 
	array(
		"ALLOW_MULTI_SELECT" => "N",
		"CHILD_MENU_TYPE" => "left",
		"COMPONENT_TEMPLATE" => "dolcar_multilevel",
		"DELAY" => "N",
		"MAX_LEVEL" => "1",
		"MENU_CACHE_GET_VARS" => array(
		),
		"MENU_CACHE_TIME" => "3600",
		"MENU_CACHE_TYPE" => "N",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"ROOT_MENU_TYPE" => "top",
		"USE_EXT" => "N"
	),
	false
);?>

  <div class="navbar-end">

                    <div class="navbar-item">
                        <div class="field is-grouped">
                            <p class="control">
                                <a class="bd-tw-button button" data-social-network="Twitter" data-social-action="tweet" data-social-target="http://localhost:4000" target="_blank" href="https://twitter.com/intent/tweet?text=Bulma: a modern CSS framework based on Flexbox&amp;hashtags=bulmaio&amp;url=http://localhost:4000&amp;via=jgthms">
              <span class="icon">
                <i class="fas fa-phone"></i>
              </span>
                                    <span>
              +7(495)25-500-25
              </span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>
</section>


<section class="section filter-top">
    <div class="container is-fullhd">
        <?$APPLICATION->IncludeComponent(
            "maxposter:api.filter",
            ".default",
            array(
                "COMPONENT_TEMPLATE" => ".default",
                "MAX_API_LOGIN" => "267",
                "MAX_API_PASSWORD" => "dolauto267",
                "URL_TEMPLATES_INDEX" => "/avtomobili-s-probegom/",
                "SET_STATUS_404" => "N",
                "SET_TITLE" => "Y"
            ),
            false
        );?>
    </div>
</section>

<!--========================================================
                          CONTENT
=========================================================-->
<section id="content">
    <div class="container">
    <div class="width-wrapper width-wrapper__inset1">
        <div class="wrapper1">
            <div class="">


                    <? if (CSite::InDir(SITE_DIR."buy/index.php") || CSite::InDir(SITE_DIR."sale/index.php") || CSite::InDir(SITE_DIR."avtomobili-s-probegom/vehicle.php") || CSite::InDir(SITE_DIR."index.php") || CSite::InDir(SITE_DIR."avtomobili-s-probegom/index.php") || CSite::InDir(SITE_DIR."special_technics") || CSite::InDir(SITE_DIR."service"))
                    {?>
                    <div class="container-margin">
                        <?}
                        else {
                        ?> <div class="container-margin"> <?
}
?>