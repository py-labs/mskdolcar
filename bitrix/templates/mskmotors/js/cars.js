var cars = {

    /*Audi*/   '43': {

        '117': [
            'A1',
        ],
        '118': [
            'A3',
        ],
        '119': [
            'A4',
        ],
        '120': [
            'A4 Allroad',
        ],
        '121': [
            'A5',
        ],
        '122': [
            'A5 Q',
        ],
        '123': [
            'A6',
        ],
        '124': [
            'A6 Q',
        ],
        '125': [
            'A6 allroad Q',
        ],
        '126': [
            'A7 Sportback',
        ],
        '127': [
            'A8 QL',
        ],
        '128': [
            'Allroad',
        ],
        '129': [
            'Q3',
        ],
        '130': [
            'Q5',
        ],
        '131': [
            'Q7',
        ],
        '132': [
            'RS Q3',
        ],
        '133': [
            'RS5',
        ],
        '134': [
            'S8 Q',
        ],




    },

    /*BMW*/   '52': {

        '44': [
            'X5',
        ],
        '53': [
            'X3',
        ],
        '78': [
            '116',
        ],
        '79': [
            '118',
        ],
        '80': [
            '118i',
        ],
        '81': [
            '120',
        ],
        '82': [
            '125',
        ],
        '83': [
            '316',
        ],
        '84': [
            '318',
        ],
        '85': [
            '320',
        ],
        '86': [
            '320D GT',
        ],
        '87': [
            '325Xi',
        ],
        '88': [
            '328',
        ],
        '89': [
            '328 i GT',
        ],
        '90': [
            '420 Gran Coupe',
        ],
        '91': [
            '435 Xi',
        ],
        '92': [
            '520',
        ],
        '93': [
            '525 Xdrive',
        ],
        '94': [
            '528 Xi',
        ],
        '95': [
            '530 GT',
        ],
        '96': [
            '550 GT',
        ],
        '97': [
            '630',
        ],
        '98': [
            '640i',
        ],
        '99': [
            '640 xd',
        ],
        '100': [
            '640 Xi',
        ],
        '101': [
            '645',
        ],
        '102': [
            '650',
        ],
        '103': [
            '730',
        ],
        '104': [
            '740',
        ],
        '105': [
            '740 Li',
        ],
        '106': [
            '750',
        ],
        '107': [
            'M5',
        ],
        '108': [
            'X1',
        ],
        '109': [
            'X1 2.5l',
        ],
        '111': [
            'X4',
        ],
        '113': [
            'X5M',
        ],
        '114': [
            'X6',
        ],
        '115': [
            'X6M',
        ],
        '116': [
            'Z4',
        ]

    },
    /*Cadillac*/   '75': {

        '135': [
            'ATS',
        ],
        '136': [
            'CTS',
        ],
        '137': [
            'Escalade',
        ],
        '138': [
            'Escalade IV',
        ],
        '139': [
            'SRX',
        ],
        '140': [
            'XT5',
        ],

    },

    /*Chevrolet*/   '76': {

        '141': [
            'Aveo',
        ],
        '142': [
            'Captiva',
        ],
        '143': [
            'Cobalt',
        ],
        '144': [
            'Cruze',
        ],
        '145': [
            'Express',
        ],
        '146': [
            'Lacetti',
        ],
        '147': [
            'Niva',
        ],
        '148': [
            'Orlando',
        ],
        '149': [
            'Spark',
        ],
        '150': [
            'Tahoe',
        ],
        '151': [
            'Tahoe IV',
        ],
        '152': [
            'TrailBlazer',
        ],

    },
    '77': {

        '153': [
            'C-Crosser',
        ],
        '154': [
            'C-Elysee',
        ],
        '155': [
            'C3',
        ],
        '156': [
            'C3 Picasso',
        ],
        '157': [
            'C4',
        ],
        '158': [
            'C4 Aircross',
        ],
        '159': [
            'C4 Picasso',
        ],
        '160': [
            'C5',
        ],
        '161': [
            'DS3',
        ],
        '162': [
            'Jumpi',
        ],
        '163': [
            'DS4',
        ],
        '164': [
            'DS5',
        ],

    }


}
