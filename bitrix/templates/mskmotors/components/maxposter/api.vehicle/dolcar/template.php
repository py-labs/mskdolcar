<!-- fotorama.css & fotorama.js. -->
<link href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script><?php
/* @class CBitrixComponentTemplate */
/**
 * 0 => string 'arResult' (length=8)
 * 1 => string 'arParams' (length=8)
 * 2 => string 'parentTemplateFolder' (length=20)
 * 3 => string 'APPLICATION' (length=11)
 * 4 => string 'USER' (length=4)
 * 5 => string 'DB' (length=2)
 * 6 => string 'templateName' (length=12)
 * 7 => string 'templateFile' (length=12)
 * 8 => string 'templateFolder' (length=14)
 * 9 => string 'componentPath' (length=13)
 * 10 => string 'component' (length=9)
 * 11 => string 'templateData' (length=12)
 **/
#var_dump(array_keys(get_defined_vars()));
?>
<div class="carblock">
    <div class="row">
        <div class="col-lg-8 col-md-7 col-sm-6">
            <?php if ($arResult['PHOTOS']) : ?>
                <div class="fotorama" data-nav="thumbs" data-allowfullscreen="true" data-ratio="4/3" data-loop="true">
                    <?php foreach ($arResult['PHOTOS'] as $photo) : ?>
                        <a href="<? echo $photo['BIG']['1'] ?>" data-full="<? echo $photo['ORIG']['1'] ?>"><img src="<? echo $photo['SML']['1'] ?>" width="<? echo $photo['SML']['2'] ?>" height="<?php echo $photo['SML']['3'] ?>"></a>
                    <?php endforeach ?>
                </div>
            <?php else : ?>
                <div class="images_block">
                    <img src="/bitrix/images/maxposter/no-image.png" width="500" height="375"/>
                </div>
            <?php endif ?>
        </div>
        <div class="col-lg-4 col-md-5 col-sm-6">
            <div class="element_params">
                <div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><h3><?php echo $arResult['MARK_NAME'] ?><?php echo $arResult['MODEL_NAME'] ?></h3></div>
                <div class="element_line year-detail"><p><?php echo GetMessage('MAX_YEAR') ?></p> <span><?php echo $arResult['YEAR'] ?></span></div>
                <div class="element_line"><p><?php echo GetMessage('MAX_DISTANCE') ?></p> <span><?php echo number_format($arResult['DISTANCE'], '0', '.', ' '), ' ', ('km' == $arResult['DISTANCE_UNIT'] ? 'км' : 'миль'); ?></span></div>
                <div class="element_line"><p><?php echo GetMessage('MAX_BODY_TYPE') ?></p> <span><?php echo $arResult['BODY_TYPE'] ?></span></div>
                <div class="element_line"><p><?php echo GetMessage('MAX_ENGINE_TYPE') ?></p> <span><?php echo $arResult['ENGINE_TYPE'] ?></span></div>
                <div class="element_line"><p><?php echo GetMessage('MAX_ENGINE_VOLUME') ?></p> <span><?php echo $arResult['ENGINE_VOLUME'] ?>&nbsp;см&#179;<?php echo($arResult['ENGINE_POWER'] ? sprintf('&nbsp;(%s&nbsp;л.с.)', $arResult['ENGINE_POWER']) : '') ?></span></div>
                <div class="element_line"><p><?php echo GetMessage('MAX_DRIVE_TYPE') ?></p> <span><?php echo $arResult['DRIVE_TYPE'] ?></span></div>
                <div class="element_line"><p><?php echo GetMessage('MAX_GEARBOX_TYPE') ?></p> <span><?php echo $arResult['GEARBOX_TYPE'] ?></span></div>
                <div class="element_line"><p><?php echo GetMessage('MAX_WHEEL_PLACE') ?></p> <span><?php echo $arResult['WHEEL_PLACE'] ?></span></div>
                <?php if ('Y' == $arParams['SHOW_COLOR']) : ?>
                    <div class="element_line"><p><?php echo GetMessage('MAX_BODY_COLOR') ?></p> <span><?php echo $arResult['BODY_COLOR'] ?></span></div>
                <?php endif ?>
                <?php if ('Y' == $arParams['SHOW_CONDITION']) : ?>
                    <div class="element_line"><p><?php echo GetMessage('MAX_CONDITION') ?></p> <span><?php echo $arResult['CONDITION'] ?></span></div>
                <?php endif ?>
                <?php if ('Y' == $arParams['SHOW_PTS']) : ?>
                    <div class="element_line"><p><?php echo GetMessage('MAX_PTS_OWNERS') ?></p> <span><?php echo $arResult['PTS_OWNERS'] ?></span></div>
                <?php endif ?>
                <?php if ('Y' == $arParams['SHOW_CUSTOMS']) : ?>
                    <div class="element_line"><p><?php echo GetMessage('MAX_PRICE_NO_CUSTOMS') ?></p> <span><?php
                            echo($arResult['PRICE_NO_CUSTOMS'] ? GetMessage('MAX_PRICE_NO_CUSTOMS_FALSE') : GetMessage('MAX_PRICE_NO_CUSTOMS_TRUE'));
                            ?></span></div>
                <?php endif ?>
                <!-- <?php /*if (array_key_exists('PRICE_OLD', $arResult)) : */ ?>
                <div class="element_line" ><p><?php /*echo GetMessage('MAX_OLD_PRICE') */ ?></p> <span><span class="element_old_price"><?php /*echo maxPrice($arResult['PRICE_OLD'], $arResult['PRICE_UNIT']) */ ?></span></span></div>
            --><?php /*endif */ ?>
                <?php if ($arResult['PRICE_NEW'] != 0): ?>
                    <div class="element_line price-detail detail-old"><p><?php echo GetMessage('MAX_OLD_PRICE') ?></p> <span><span class="element_price"><?php echo maxPrice($arResult['PRICE_BASE'], $arResult['PRICE_UNIT']) ?></span></span></div>
                    <div class="element_line price-detail"><p><?php echo GetMessage('MAX_PRICE') ?></p> <span><span class="element_price"><?php echo maxPrice($arResult['PRICE_NEW'], $arResult['PRICE_UNIT']) ?></span></span></div>
                <? else: ?>
                    <div class="element_line price-detail"><p><?php echo GetMessage('MAX_PRICE') ?></p> <span><span class="element_price"><?php echo maxPrice($arResult['PRICE'], $arResult['PRICE_UNIT']) ?></span></span></div>
                <? endif; ?>

                <?php if ('Y' == $arParams['SHOW_AVAILABILITY']) : ?>
                    <div class="element_line"><p><?php echo GetMessage('MAX_AVAILABILITY') ?></p> <span><?php echo $arResult['AVAILABILITY'] ?></span></div>
                <?php endif ?>

                <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                <script src="//yastatic.net/share2/share.js"></script>
                <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir,gplus,twitter"></div>


                <script type="text/javascript">
                    $(document).ready(function () {
                        $('.fancybox').fancybox();
                    });
                </script>
                <div class="button-set">
                    <br>
                    <a href="#calculator" class="btn-std fancybox">Кредитный калькулятор</a>
                    <a href="#trade" class="btn-std fancybox">Обмен</a>
                    <br/>
                </div>
                <div class="additional-info">
                    <h5>Заинтересовал автомобиль?</h5>
                    <p>Специалист отдела продаж ответит на любые вопросы по автомобилю.</p>
                    <a href="#form_tel" class="btn-std callback" style="float: left">Заказать звонок</a>
                    <a href="tel:+7(495)25-500-25"><h4 class="tel_cart">+7(495)25-500-25</h4></a>
                </div>
            </div>
        </div>
    </div>


    <?
    $GLOBALS["price_dolcar"] = $arResult['PRICE'];
    $GLOBALS["mark_name"] = $arResult['MARK_NAME'];
    $GLOBALS["model_name"] = $arResult['MODEL_NAME'];
    $GLOBALS["year"] = $arResult['YEAR'];
    $GLOBALS["distance"] = $arResult['DISTANCE'];
    $GLOBALS["engine_volume"] = $arResult['ENGINE_VOLUME'];
    $GLOBALS["engine_type"] = $arResult['ENGINE_TYPE'];
    $GLOBALS["gearbox_type"] = $arResult['GEARBOX_TYPE'];
    $GLOBALS["vehicle_id"] = $arResult['ID'];

    ?>

    <?php
    $i = 0;
    foreach ($arResult['PHOTOS'] as $photo) :
        if ($i < 1) {
            $GLOBALS["image_credit"] = $photo['BIG']['1'];
        }
        $i++;
    endforeach ?>


</div>
<?php if ($arResult['OPTIONS']) : ?>
    <div class="box1">
        <h3 class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><?php echo GetMessage('MAX_OPTIONS_TITLE') ?></h3>
    </div>
    <div class="row">
        <?php foreach ($arResult['OPTIONS'] as $name => $option) : ?>
            <div class="col-lg-3 col-md-4 col-sm-6"><span class="li"><?php echo GetMessage('MAX_' . $name), (true === $option ? '' : ': ' . $option); ?></span>

            </div>
        <?php endforeach ?>
    </div>
<?php endif ?>

<?php if ('Y' == $arParams['SHOW_DESCRIPTION'] && !empty($arResult['DESCRIPTION'])) : ?>
    <div class="box1">
        <h3><?php echo GetMessage('MAX_DESCRIPTION') ?></h3>
        <p><?php echo $arResult['DESCRIPTION'] ?></p>
    </div>
<?php endif ?>
<?php if ('Y' == $arParams['SHOW_INSPECTION'] && !empty($arResult['INSPECTION'])) : ?>
    <div class="box1">
        <h3><?php echo GetMessage('MAX_INSPECTION') ?></h3>
        <p><?php echo $arResult['INSPECTION'] ?></p>
    </div>
<?php endif ?>

<?php if (('Y' == $arParams['SHOW_CONTACTS']) && (bool)$arResult['PHONES']) : ?>
    <div class="element_description">
        <h3><?php echo GetMessage('MAX_CONTACTS') ?></h3>
        <p><?php echo implode(';<br />', $arResult['PHONES']) ?></p>
    </div>
<?php endif ?>
<div class="clearfix"></div>

<? $APPLICATION->IncludeComponent("maxposter:api.recommend", "recommend", Array(
    "ADD_SECTIONS_CHAIN" => "Y",
    "COMPONENT_TEMPLATE" => "special_offers",
    "MAX_API_LOGIN" => "267",
    "MAX_API_PASSWORD" => "dolauto267",
    "SET_STATUS_404" => "N",
    "SET_TITLE" => "Y",
    "URL_TEMPLATES_INDEX" => "/avtomobili-s-probegom/",
    "URL_TEMPLATES_VEHICLE" => "/avtomobili-s-probegom/vehicle.php?VEHICLE_ID=#VEHICLE_ID#"
),
    false
); ?>