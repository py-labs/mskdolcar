﻿<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<!--========================================================
                          FOOTER
=========================================================-->
<footer id="footer">
  <div class="width-wrapper width-wrapper__inset1 width-wrapper__inset2">
    <div class="wrapper4">
      <div class="container">
        <div class="row">
          <div class="grid_3">
            <div class="box2">
              <h5>Cars for sale</h5>
              <ul class="list1">
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="#">Used cars for sale</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="#">Second hand cars for sale</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="#">New cars for sale</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="#">Special Offers</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s"><a href="#">Compare Cars</a></li>
              </ul>
            </div>
          </div>
          <div class="grid_3">
            <div class="box2">
              <h5>Car research</h5>
              <ul class="list1">
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="#">Research Cars</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="#">Car Valuations</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="#">Car Finance</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="#">Car Insurance</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s"><a href="#">Car Comparisons</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s"><a href="#">Car Facts</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s"><a href="#">History Reports</a></li>
              </ul>
            </div>
          </div>
          <div class="grid_3">
            <div class="box2">
              <h5>News & Reviews</h5>
              <ul class="list1">
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="#">Car News</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="#">Car Reviews</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="#">Car Videos</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="#">Car Advice</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s"><a href="#">New Car Calendar</a></li>
              </ul>
            </div>
          </div>
          <div class="grid_3">
            <div class="box2">
              <h5>Categories</h5>
              <ul class="list1">
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s"><a href="#">New cars</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s"><a href="#">Bikes</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s"><a href="#">Boats</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.4s"><a href="#">Trucks</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s"><a href="#">Caravans</a></li>
                <li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="0.6s"><a href="#">Machinery</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="wrapper5">
    <div class="width-wrapper width-wrapper__inset1 width-wrapper__inset3">
      <div class="container">
        <div class="row">
          <div class="grid_12">
            <div class="privacy-block wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
              <a href="index.html">Car sell</a> &copy; <span id="copyright-year"></span>. <a
                    href="index-5.html">Privacy policy</a>
              <!--{%FOOTER_LINK} -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

<script src="js/script.js"></script>
<script type="text/javascript">
 var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-7078796-5']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();</script>
</body>
</html>