<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<ul class="list1">
<? $tt=0.0; ?>
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
<?  $tt=$tt+0.1;?>
	<?if($arItem["SELECTED"]):?>
		<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="<?=$tt?>s"><a href="<?=$arItem["LINK"]?>" class="selected"><?=$arItem["TEXT"]?></a></li>
	<?else:?>
		<li class="wow fadeIn" data-wow-duration="1s" data-wow-delay="<?=$tt?>s"><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
	<?endif?>
	
<?endforeach?>

</ul>
<?endif?>