<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>


<div class="navbar-start">
	<a class="navbar-item" href="/avtomobili-s-probegom">
		Авто с пробегом
	</a>
	<a class="navbar-item" href="/sale">
		Продать
	</a>
	<a class="navbar-item" href="/buy">
		Купить
	</a>
	<div class="navbar-item has-dropdown is-hoverable">
		<a class="navbar-link" href="/uslugi">
			Услуги
		</a>
		<div class="navbar-dropdown is-boxed">
			<a class="navbar-item" href="/uslugi/kreditovanie">
				Кредитоварние
			</a>
			<a class="navbar-item" href="/uslugi/strahovanie">
				Страхование
			</a>
			<a class="navbar-item" href="/uslugi/oformlenie">
				Оформление
			</a>
		</div>
	</div>
	<div class="navbar-item has-dropdown is-hoverable">
		<a class="navbar-link" href="/service">
			Сервис
		</a>
		<div class="navbar-dropdown is-boxed">
			<a class="navbar-item" href="/service/remont-avtoelektroniki">
				Ремонт автоэлектроники
			</a>
			<a class="navbar-item" href="/service/friends">
				Сервис для друзей
			</a>
			<a class="navbar-item" href="http://zapdolcar.ru">
				Запчасти
			</a>
			<a class="navbar-item" href="http://dolmoto.ru">
				Мотосалон
			</a>
			<a class="navbar-item" href="http://dolcar.su">
				Доп.оборудование
			</a>
		</div>
	</div>
	<a class="navbar-item" href="/special_technics">
		Спецтехника
	</a>
	<a class="navbar-item" href="/contacts">
		Контакты
	</a>
</div>


<?/*
$previousLevel = 0;
foreach($arResult as $arItem):?>

	<?if ($previousLevel && $arItem["DEPTH_LEVEL"] < $previousLevel):?>
		<?=str_repeat("</ul></li>", ($previousLevel - $arItem["DEPTH_LEVEL"]));?>
	<?endif?>

	<?if ($arItem["IS_PARENT"]):?>

		<?if ($arItem["DEPTH_LEVEL"] == 1):?>
			<li><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a>
				<ul>
		<?else:?>
			<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?=$arItem["LINK"]?>" class="parent"><?=$arItem["TEXT"]?></a>
				<ul>
		<?endif?>

	<?else:?>

		<?if ($arItem["PERMISSION"] > "D"):?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li><a href="<?=$arItem["LINK"]?>" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li<?if ($arItem["SELECTED"]):?> class="item-selected"<?endif?>><a href="<?=$arItem["LINK"]?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?else:?>

			<?if ($arItem["DEPTH_LEVEL"] == 1):?>
				<li><a href="" class="<?if ($arItem["SELECTED"]):?>root-item-selected<?else:?>root-item<?endif?>" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?else:?>
				<li><a href="" class="denied" title="<?=GetMessage("MENU_ITEM_ACCESS_DENIED")?>"><?=$arItem["TEXT"]?></a></li>
			<?endif?>

		<?endif?>

	<?endif?>

	<?$previousLevel = $arItem["DEPTH_LEVEL"];?>

<?endforeach?>

<?if ($previousLevel > 1)://close last item tags?>
	<?=str_repeat("</ul></li>", ($previousLevel-1) );?>
<?endif?>

</ul>
<div class="menu-clear-left"></div>

*/?>

<?endif?> 
