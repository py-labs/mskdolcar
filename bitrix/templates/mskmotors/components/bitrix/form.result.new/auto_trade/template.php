<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<?if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>

<? if( $arResult["FORM_NOTE"] ) { ?>
    <?//=$arResult["FORM_NOTE"];?>
    <div class="otp_otz" data-otz="30"></div>
<? } ?>

<?if ($arResult["isFormNote"] != "Y")
{
    ?>
    <?=$arResult["FORM_HEADER"]?>

    <table style="width: 100%;">
        <?
        if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y")
        {
            ?>
            <tr>
                <td><?
                    /***********************************************************************************
                    form header
                     ***********************************************************************************/
                    if ($arResult["isFormTitle"])
                    {
                        ?>
                        <h3><?=$arResult["FORM_TITLE"]?></h3>
                        <?
                    } //endif ;

                    if ($arResult["isFormImage"] == "Y")
                    {
                        ?>
                        <a href="<?=$arResult["FORM_IMAGE"]["URL"]?>" target="_blank" alt="<?=GetMessage("FORM_ENLARGE")?>"><img src="<?=$arResult["FORM_IMAGE"]["URL"]?>" <?if($arResult["FORM_IMAGE"]["WIDTH"] > 300):?>width="300"<?elseif($arResult["FORM_IMAGE"]["HEIGHT"] > 200):?>height="200"<?else:?><?=$arResult["FORM_IMAGE"]["ATTR"]?><?endif;?> hspace="3" vscape="3" border="0" /></a>
                        <?//=$arResult["FORM_IMAGE"]["HTML_CODE"]?>
                        <?
                    } //endif
                    ?>
                    <? if( $arResult["FORM_DESCRIPTION"] ){ ?>
                        <p><?/*=$arResult["FORM_DESCRIPTION"]*/?></p>
                        <p>Какой у Вас автомобиль</p>
                    <? } ?>
                </td>
            </tr>
            <?
        } // endif
        ?>
    </table>
    <br />
    <?
    /***********************************************************************************
    form questions
     ***********************************************************************************/
    ?>
    <table class="form-table data-table">
        <thead style="display: none;">
        <tr>
            <th colspan="2">&nbsp;</th>
        </tr>
        </thead>
        <tbody>

        <script>
            $(document).ready(function() {
                $('.model').wrapAll('<div id="wrapperTypeBrand">');
            });
        </script>

        <script>
            (function(){
                var cars = {

                    '43': {

                        '54': [
                            'Audi X7',
                        ],
                        '55': [
                            'Audi T-800',
                        ]
                    },

                    '52': {

                        '44': [
                            'Ford Harrison',
                        ],
                        '53': [
                            'Ford Henry',
                        ]
                    }

                }



                function changeBrand(){

                    var brand = document.querySelector('#form_dropdown_SIMPLE_QUESTION_346');
                    var wrTypeBrand = document.querySelector('#wrapperTypeBrand');
                    console.log(brand);
                    console.log(wrTypeBrand);
                    console.log(cars);
                    var str = '<select id="typeBrand" class="form-control" name="form_dropdown_SIMPLE_QUESTION_813">';
                    var type = cars[brand.value];
                    for(var e in type)
                        str += '<option value="' + e + '">' + type[e][0] + '</option>';
                    str += '</select>';
                    wrTypeBrand.innerHTML = str;
                    setTimeout(function(){
                        changeTypeBrand();
                        document.querySelector('#typeBrand').addEventListener('change', changeTypeBrand);
                    }, 0);
                }



                function changeTypeBrand(){
                    var brand = document.querySelector('#form_dropdown_SIMPLE_QUESTION_346').value;
                    var type = document.querySelector('#typeBrand').value;

                }

                function load(){
                    changeBrand();
                    document.querySelector('#form_dropdown_SIMPLE_QUESTION_346').addEventListener('change', changeBrand);
                }

                document.addEventListener('DOMContentLoaded', load);
            })();

        </script>
        <div class="row">
            <div class="col-lg-12">

                <?
                foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                    if ($arQuestion["STRUCTURE"][0]["ID"] != 44):

                        //var_dump($arQuestion["HTML_CODE"]);
                        if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
                            echo $arQuestion["HTML_CODE"];
                        } else {
                            ?>

                            <?if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] != 'checkbox'):?>
                            <div class="col-lg-6">
                                <label for="priceto"><?= $arQuestion["CAPTION"] ?></label>
                                <?= $arQuestion["HTML_CODE"] ?>
                            </div>
                            <?else:?>
                                <div class="col-lg-12" style="margin-top: 20px">
                                <label for="priceto"><?= $arQuestion["CAPTION"] ?></label>
                                <?= $arQuestion["HTML_CODE"] ?>
                            </div>
                            <?endif;?>



                            <?
                        }


                    else:
                        //var_dump($arQuestion["HTML_CODE"]);
                        if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
                            echo $arQuestion["HTML_CODE"];
                        } else {
                            ?>
                            <div class="col-lg-6">
                                <label for="priceto"><?= $arQuestion["CAPTION"] ?></label>
                                <div id='wrapperTypeBrand' class="my"></div>
                            </div>
                            <?
                        }
                    endif;

                } //endwhile

                ?>


                <?
                if($arResult["isUseCaptcha"] == "Y")
                {
                    ?>
                    <tr>
                        <th colspan="2"><b><?=GetMessage("FORM_CAPTCHA_TABLE_TITLE")?></b></th>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td><input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" /></td>
                    </tr>
                    <tr>
                        <td><?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?></td>
                        <td><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" /></td>
                    </tr>
                    <?
                } // isUseCaptcha
                ?>

                <div class="row">
                    <div class="col-lg-12">
                        <input <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" class="submit-button button-auto-request" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
                        <?if ($arResult["F_RIGHT"] >= 15):?>
                            &nbsp;<input type="hidden" name="web_form_apply" value="Y" />
                            <?/* <input type="submit" name="web_form_apply" value="<?=GetMessage("FORM_APPLY")?>" />  */?>
                        <?endif;?>
                        <?/*  &nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET");?>" />   */?>
                    </div>


                </div>

            </div>


        </div>


        </tbody>



    </table>
   <!-- <p>

        <script>
            var adrSend_1 = window.location.href;
            $("#form_question input[name=form_text_28]").val(adrSend_1);
            $("#form_question input[name=form_text_28]").closest("tr").hide();
        </script>

        <?/*=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")*/?>
    </p>-->
    <?=$arResult["FORM_FOOTER"]?>
    <?
} //endif (isFormNote)


?>