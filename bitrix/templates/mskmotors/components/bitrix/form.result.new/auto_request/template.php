<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
?>

<? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>

<? if ($arResult["FORM_NOTE"]) { ?>
    <? //=$arResult["FORM_NOTE"];?>
    <div class="otp_otz" data-otz="10"></div>
<? } ?>

<? if ($arResult["isFormNote"] != "Y") {
    ?>
<form name="<?= $arResult["WEB_FORM_NAME"] ?>" action="<?= POST_FORM_ACTION_URI ?>" method="POST" enctype="multipart/form-data" class="auto-request" novalidate="novalidate">
    <input type="hidden" name="WEB_FORM_ID" value="<?= $arParams["WEB_FORM_ID"] ?>">
    <?= bitrix_sessid_post() ?>

    <table style="width: 100%;">
        <?
        if ($arResult["isFormDescription"] == "Y" || $arResult["isFormTitle"] == "Y" || $arResult["isFormImage"] == "Y") {
            ?>
            <tr>
                <td><?
                    /***********************************************************************************
                     * form header
                     ***********************************************************************************/
                    if ($arResult["isFormTitle"]) {
                        ?>
                        <h3><?= $arResult["FORM_TITLE"] ?></h3>
                        <?
                    } //endif ;

                    if ($arResult["isFormImage"] == "Y") {
                        ?>
                        <a href="<?= $arResult["FORM_IMAGE"]["URL"] ?>" target="_blank" alt="<?= GetMessage("FORM_ENLARGE") ?>"><img src="<?= $arResult["FORM_IMAGE"]["URL"] ?>" <? if ($arResult["FORM_IMAGE"]["WIDTH"] > 300): ?>width="300" <? elseif ($arResult["FORM_IMAGE"]["HEIGHT"] > 200): ?>height="200"<? else: ?><?= $arResult["FORM_IMAGE"]["ATTR"] ?><? endif;
                            ?> hspace="3" vscape="3" border="0"/></a>
                        <? //=$arResult["FORM_IMAGE"]["HTML_CODE"]
                        ?>
                        <?
                    } //endif
                    ?>
                    <? if ($arResult["FORM_DESCRIPTION"]) { ?>
                        <p><?= $arResult["FORM_DESCRIPTION"] ?></p>
                    <? } ?>
                </td>
            </tr>
            <?
        } // endif
        ?>
    </table>
    <br/>
    <?
    /***********************************************************************************
     * form questions
     ***********************************************************************************/
    ?>
    <table class="form-table data-table">
        <thead style="display: none;">
        <tr>
            <th colspan="2">&nbsp;</th>
        </tr>
        </thead>
        <tbody>


        <script>
            $(document).ready(function () {
                $('.model').wrapAll('<div id="wrapperTypeBrand">');
            });
        </script>
        <div class="row">
            <div class="col-md-6 col-md-push-6">
                <div class="row">
                    <?
                    foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion) {
                        if ($arQuestion["STRUCTURE"][0]["ID"] != 994):

                            //var_dump($arQuestion["HTML_CODE"]);
                            if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
                                echo $arQuestion["HTML_CODE"];
                            } else {
                                ?>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="priceto"><?= $arQuestion["CAPTION"] ?></label>
                                        <?= $arQuestion["HTML_CODE"] ?>
                                    </div>
                                </div>
                                <?
                            }
                        else:
                            //var_dump($arQuestion["HTML_CODE"]);
                            if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden') {
                                echo $arQuestion["HTML_CODE"];
                            } else {
                                ?>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="priceto"><?= $arQuestion["CAPTION"] ?></label>
                                        <div id='wrapperTypeBrand' class="my"></div>
                                    </div>
                                </div>
                                <?
                            }
                        endif;

                    } //endwhile

                    ?>
                    <?
                    if ($arResult["isUseCaptcha"] == "Y") {
                        ?>
                        <tr>
                            <th colspan="2"><b><?= GetMessage("FORM_CAPTCHA_TABLE_TITLE") ?></b></th>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td><input type="hidden" name="captcha_sid" value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/><img src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>" width="180" height="40"/></td>
                        </tr>
                        <tr>
                            <td><?= GetMessage("FORM_CAPTCHA_FIELD_TITLE") ?><?= $arResult["REQUIRED_SIGN"]; ?></td>
                            <td><input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext"/></td>
                        </tr>
                        <?
                    } // isUseCaptcha
                    ?>


                    <div class="col-xs-12">
                        <div class="form-group">
                            <input <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?> type="submit" name="web_form_submit" class="btn-big btn-maxposter button-auto-request" value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"/>
                            <? if ($arResult["F_RIGHT"] >= 15): ?>
                                &nbsp;<input type="hidden" name="web_form_apply" value="Y"/>
                                <? /* <input type="submit" name="web_form_apply" value="<?=GetMessage("FORM_APPLY")?>" />  */
                                ?>
                            <? endif; ?>
                            <? /*  &nbsp;<input type="reset" value="<?=GetMessage("FORM_RESET");?>" />   */
                            ?>
                        </div>


                    </div>
                </div>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <? $APPLICATION->IncludeComponent("bitrix:main.include", "template1", Array(
                    "AREA_FILE_SHOW" => "file",    // Показывать включаемую область
                    "AREA_FILE_SUFFIX" => "inc",
                    "COMPONENT_TEMPLATE" => ".default",
                    "EDIT_TEMPLATE" => "",    // Шаблон области по умолчанию
                    "PATH" => "/include/buy_area.php",    // Путь к файлу области
                ),
                    false
                ); ?>
            </div>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('.fancybox').fancybox();
                });
            </script>


            <div class="col-lg-1"></div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div>
                    <p>
                        <font color="red"><span class="form-required starrequired">*</span></font> - обязательные поля</p>
                    <p>Если у Вас есть вопросы задайте их нашему менеджеру <a href="#form_question" class=" fancybox">Задать вопрос</a></p>
                    <p>Вы так же можете ознакомиться с каталогом автомобилей, представленных на нашем сайте <a href="/avtomobili-s-probegom/">все автомобили в наличии</a></p>
                </div>
            </div>
            <div class="col-lg-6"></div>
        </div>


        </tbody>


    </table>
    <!--
    <p>

        <script>
            var adrSend_1 = window.location.href;
            /*$("#form_question input[name=form_text_28]").val(adrSend_1);
            $("#form_question input[name=form_text_28]").closest("tr").hide();*/
        </script>

        <? /*=$arResult["REQUIRED_SIGN"];?> - <?=GetMessage("FORM_REQUIRED_FIELDS")*/
    ?>
    </p>-->
    <?= $arResult["FORM_FOOTER"] ?>
    <?
} //endif (isFormNote)


?>