<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(false);

if (!empty($arResult["ERRORS"])):?>
	<?ShowError(implode("<br />", $arResult["ERRORS"]))?>
<?endif;
if (strlen($arResult["MESSAGE"]) > 0):?>
	<?ShowNote($arResult["MESSAGE"])?>
<?endif?>

<style>
	.open-form {
		background-color: white !important;
		border-color: #ed642f !important;
		color: #ed642f !important;
	}
	.open-form:hover {
		background-color: #ed642f !important;
		color: white !important;
	}
	.submit-review {
		background-color: #303030 !important;
		color: white !important;
	}
</style>

<div id="form-review-section">


<form id="review-form" name="iblock_add" @submit="checkForm" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
	<?=bitrix_sessid_post()?>
	<div class="heading1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
<a class="button is-success open-form is-outlined" v-on:click="show = !show">Оставить отзыв</a>
	<h2>Отзывы клиентов</h2>
</div>

	<div v-if="show">
	
	<?if ($arParams["MAX_FILE_SIZE"] > 0):?><input type="hidden" name="MAX_FILE_SIZE" value="<?=$arParams["MAX_FILE_SIZE"]?>" /><?endif?>
	<div class="form-review">
		<div class="columns">
			<div class="column is-three-fifths">
				
				<div class="columns">
					<div class="column is-one-third">
						<div class="field">
							<div class="control">
								<input class="input" type="text" name="PROPERTY[NAME][0]" placeholder="Имя" v-model="name">
							</div>
						</div>
					</div>
					<div class="column is-one-third">
					<div class="field">
							<div class="control">
								<input class="input"  type="text" name="captcha_word" maxlength="50" value="" placeholder="Код с изображения" v-model="captcha">
							</div>
						</div>
					</div>
					<div class="column is-one-third">
					<div class="field">
							<div class="control">
							<input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
							<img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
							</div>
						</div>
					</div>
				</div>
				<div class="field">
							<div class="control">
								<textarea class="textarea" name="PROPERTY[PREVIEW_TEXT][0]" placeholder="Отзыв" v-model="text"></textarea>
							</div>
						</div>
				<div class="field is-grouped">
					<div class="control">
					<input class="button is-link submit-review" type="submit" name="iblock_submit" value="<?=GetMessage("IBLOCK_FORM_SUBMIT")?>" />
					<?if (strlen($arParams["LIST_URL"]) > 0):?>
						<input
							type="button"
							name="iblock_cancel"
							value="<? echo GetMessage('IBLOCK_FORM_CANCEL'); ?>"
							onclick="location.href='<? echo CUtil::JSEscape($arParams["LIST_URL"])?>';"
						>
					<?endif?>
					</div>
				</div>
			</div>
			<div class="column is-one-third">
				<div v-if="errors.length" class="notification is-danger">
				<b>Пожалуйста, исправьте следующие ошибки:</b>
				<ul>
				<li class="review-form-error" v-for="error in errors">{{ error }}</li>
				</ul>
				</div>
			</div>
		</div>
    	</div>
		</div>
</form>
</div>

<script>
    const reviewForm = new Vue({
  el:'#review-form',
  data:{
	  errors:[],
      name:null,
	  text:null,
      captcha:null,
	  show: false
  },
  methods:{
    checkForm:function(e) {
      if(this.name && this.text && this.captcha) return true;
      this.errors = [];
      if(!this.name) this.errors.push("Поле 'Имя' обязательно для заполнения.");
      if(!this.text) this.errors.push("Поле 'Отзыв' обязательно для заполнения.");
	  if(!this.captcha) this.errors.push("Поле 'Код с изображения' обязательно для заполнения.");
      e.preventDefault();
    }
  }
});
</script>