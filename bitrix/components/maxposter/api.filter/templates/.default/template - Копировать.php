﻿<?php
/* @class CBitrixComponentTemplate */
/**
  0 => string 'arResult' (length=8)
  1 => string 'arParams' (length=8)
  2 => string 'parentTemplateFolder' (length=20)
  3 => string 'APPLICATION' (length=11)
  4 => string 'USER' (length=4)
  5 => string 'DB' (length=2)
  6 => string 'templateName' (length=12)
  7 => string 'templateFile' (length=12)
  8 => string 'templateFolder' (length=14)
  9 => string 'componentPath' (length=13)
  10 => string 'component' (length=9)
  11 => string 'templateData' (length=12)
**/

/* var_dump($arResult); */
?>



<form method="POST" action="<?=$arResult['URL']?>" id="max-filter" class="bookingForm1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.1s">
<label class="heading" for="<?=$arResult['FORM']['[mark_id]']['ID'] ?>">Марка:</label>
	<select name="FS<?=$arResult['FORM']['[mark_id]']['NAME'] ?>" id="<?=$arResult['FORM']['[mark_id]']['ID']?>" class="tmSelect auto">
		<option>Любая марка</option>
        <? foreach ($arResult['FORM']['[mark_id]']['OPTIONS'] as $option) : ?>
        <option value="<?=$option['NAME']?>"<?=($arResult['SELECTED']['mark_id'] == $option['NAME'] ? ' selected="selected"' : '')?>><?=$option['VALUE']?></option>
        <? endforeach ?>
	</select>
<label class="heading" for="<?=$arResult['FORM']['[model_id]']['ID'] ?>">Модель</label>
	<select name="FS<?=$arResult['FORM']['[model_id]']['NAME'] ?>" id="<?=$arResult['FORM']['[model_id]']['ID']?>" class="tmSelect auto">
        <option>Любая модель</option>
        <? foreach ($arResult['FORM']['[model_id]']['OPTIONS'] as $option) : ?>
        <option value="<?=$option['NAME'] ?>"<?=($arResult['SELECTED']['model_id'] == $option['NAME'] ? ' selected="selected"' : '')?> class="mark-<?=$option['PARENT'] ?>"><?=$option['VALUE'] ?></option>
        <? endforeach ?>
    </select>
								<div class="narrow-selects">
									<div class="block-left">
 <span class="heading">Min Year:</span>
										<select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
											<option>Min</option>
											<option>Min 1</option>
											<option>Min 2</option>
											<option>Min 3</option>
											<option>Min 4</option>
											<option>Min 5</option>
										</select>
									</div>
									<div class="block-right">
 <span class="heading">Max Year:</span>
										<select name="Make" class="tmSelect auto" data-class="tmSelect tmSelect2" data-constraints="@Required">
											<option>2014</option>
											<option>2013</option>
											<option>2012</option>
											<option>2011</option>
											<option>2010</option>
											<option>2009</option>
										</select>
									</div>
									<div class="clearfix">
									</div>
									<div class="block-left">
	            <label class="heading" for="FS<?php echo $arResult['FORM']['[price][from]']['ID'] ?>">Цена от</label>
                <input class="tmSelect auto"
                    type="text"
                    size="9"
                    name="FS<?php echo $arResult['FORM']['[price][from]']['NAME'] ?>"
                    value="<?php echo $arResult['SELECTED']['price']['from'] ?>"
                    id="FS<?php echo $arResult['FORM']['[price][from]']['ID'] ?>" />
									</div>
									<div class="block-right">
                <label class="heading" for="FS<?php echo $arResult['FORM']['[price][to]']['ID'] ?>">до</label>
                <input class="tmSelect auto"
                        type="text"
                        size="9"
                        name="FS<?php echo $arResult['FORM']['[price][to]']['NAME'] ?>"
                        value="<?php echo $arResult['SELECTED']['price']['to'] ?>"
                        id="FS<?php echo $arResult['FORM']['[price][to]']['ID'] ?>" />
									</div>
									<div class="clearfix">
									</div>
								</div>

                <input type="submit" value="Искать" />
                &nbsp;
                <input type="reset" value="Сбросить" />
</form>
