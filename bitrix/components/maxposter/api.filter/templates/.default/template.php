﻿<?
/* @class CBitrixComponentTemplate */
/**
0 => string 'arResult' (length=8)
1 => string 'arParams' (length=8)
2 => string 'parentTemplateFolder' (length=20)
3 => string 'APPLICATION' (length=11)
4 => string 'USER' (length=4)
5 => string 'DB' (length=2)
6 => string 'templateName' (length=12)
7 => string 'templateFile' (length=12)
8 => string 'templateFolder' (length=14)
9 => string 'componentPath' (length=13)
10 => string 'component' (length=9)
11 => string 'templateData' (length=12)
 **/

?>


<style>
    .filter-form .row{
        margin-left: -5px;
        margin-right: -5px;
    }
    .filter-form [class*="col-"] {
        padding-left: 5px;
        padding-right: 5px;
    }

    #finput {
        max-width:173px
    }

    .control > input::placeholder {
        color: #777777;
    }

    .container-filter {
        max-width:1174px !important;
        width: 1174px !important;
    }

    .filter-section {
        background-color: #ed642f;
        padding-bottom: 0px;
        padding-left: 71px;
        padding-top:20px;
    }

    .button-filter {
        height: 33px;
        display: block;
        width: 100%;
    }

    .button-filter-all {
        height: 33px;
        margin-right:5px;
        float:left
    }

    .filter-form {
        padding: 0px 0px 0px 3px;
    }
    .field-filter,
    .field-filter select,
    .field-filter input{
        width: 100%;
    }
    [v-cloak] {
        display: none;
    }
    #finput.field-filter{
        max-width: 100%;
    }
    @media (max-width: 1199px) {
        .field-filter {
            margin-bottom: 10px;
        }
    }
</style>

    <form method="POST" action="<?php echo $arResult['URL'] ?>" id="max-filter" class="filter-form bookingForm1 wow fadeIn" data-wow-duration="1s" data-wow-delay="0.3s">
        <div id="app" class="row">
            <div class="col-lg-2 col-sm-4 col-xs-6">
        <div class="select field-filter">
            <select v-model="selectedMark" @change="selectMark" name="FS[mark_id]">
               <option v-cloak v-for="mark in marks" :value="mark.mark.id">{{ mark.mark.title }}</option>
            </select>
        </div>
            </div>
            <div class="col-lg-2 col-sm-4 col-xs-6">
        <div class="select field-filter">
            <select v-model="selectedModel" name="FS[model_id]">
              <option v-cloak v-for="model in models" :value="model.id">{{ model.title }}</option>
            </select>
        </div>
            </div>

            <div class="col-lg-1 col-sm-4 col-xs-6">
        <div class="select field-filter">
            <select v-model="selectedGear" name="FS[gearbox_type]">
              <option v-cloak v-for="gear in gears" :value="gear.value">{{ gear.title }}</option>
            </select>
        </div>
            </div>

            <div class="col-lg-1 col-sm-2 col-xs-6">
        <div class="select field-filter">
            <select v-model="selectedYearFrom" name="FS[year][from]">
              <option v-cloak v-for="year in yearsFrom" :value="year.id">{{ year.title }}</option>
            </select>
        </div>
            </div>
            <div class="col-lg-1 col-sm-2 col-xs-6">
        <div class="select field-filter">
            <select v-model="selectedYearTo" name="FS[year][to]">
              <option v-cloak v-for="year in yearsTo" :value="year.id">{{ year.title }}</option>
            </select>
        </div>
            </div>
            <div class="col-sm-2 col-xs-6">
         <div class="field field-filter" id="finput">
            <div class="control">
                <input class="input" v-model="selectedPriceFrom" type="text" name="FS[price][from]" placeholder="Цена от">
            </div>
        </div>
            </div>
            <div class="col-sm-2 col-xs-6">
        <div class="field field-filter" id="finput">
        <div class="control">
            <input class="input" v-model="selectedPriceTo" type="text" name="FS[price][to]" placeholder="Цена до">
        </div>
        </div>
            </div>

            <div class="col-lg-1 col-sm-4 col-xs-6">
            <button class="button-filter button is-dark">Искать</button>
            </div>

    </div>
    </form>

<script>
const store = new Vuex.Store({
  state: {
    selectedMark: -1
  },
  mutations: {
    increment (state, payload) {
        state.selectedMark = payload.mark
    }
  },
  getters: {
    mark(state) {
        return state.selectedMark
    }
}
});

    let cars  = <?=json_encode($arResult['AUT'])?>

    const app = new Vue({
  el:'#app',
  data:cars,
  store,
  computed: {
    selectedMark() {
            return this.$store.getters.mark;
        }
    },
  methods:{
    selectMark:function() {
        marks = this.marks.find(item => {
            return item.mark.id == this.selectedMark
        });
        this.models = marks.models;
        this.selectedModel = -1;
        store.commit({
            type: 'increment',
            mark: this.selectedMark
        });
    },
    reload:function() {
        marks = this.marks.find(item => {
            return item.mark.id == this.selectedMark
        });

        if (this.selectedModel != -1) {
            store.commit({
            type: 'increment',
            mark: this.selectedMark
        });
        this.models = marks.models;
        }

        if (this.selectedMark == -1) {
            this.selectedModel = -1;
        } else {
            this.models = marks.models;
        }
    }
}
});

app.reload();
</script>