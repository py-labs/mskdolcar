<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/* @var $arParams array */
/* @var $arResult array */
/* @var $APPLICATION CMain */
/* @var $this CBitrixComponent */

// Модуль Макспотера
$moduleId = 'maxposter.api';
if (!CModule::IncludeModule($moduleId))
{
	ShowError(GetMessage('MAXPOSTER_API_MODULE_NOT_INSTALL'));
	return;
}

// Параметры модуля
$dealerId = COption::GetOptionString($moduleId, 'MAX_API_LOGIN');
$password = COption::GetOptionString($moduleId, 'MAX_API_PASSWORD');

/**************************** параметры компонента ****************************/
if (!empty($arParams['MAX_API_LOGIN'])) {
    $dealerId = $arParams['MAX_API_LOGIN'];
}
if (!empty($arParams['MAX_API_PASSWORD'])) {
    $password = $arParams['MAX_API_PASSWORD'];
}
$arUrlDefault = array(
    'URL_TEMPLATES_INDEX'    => '',
);
foreach ($arUrlDefault as $urlKey => $url) {
    if (!array_key_exists($urlKey, $arParams) || (0 >= strlen($arParams[$urlKey]))) {
        $arParams[$urlKey] = $APPLICATION->GetCurPage() . '?' . htmlspecialcharsbx($url);
    }
}


$filterParams = (array_key_exists('FS', $_REQUEST) ? $_REQUEST['FS'] : array());

//var_dump($filterParams);

 /************************** клиент, запрос к сервису **************************/
// Параметры запроса
$client = new maxCacheXmlClient(array(
    'api_version' => 1,
    'dealer_id'   => $dealerId,
    'password'    => $password,
    'cache_dir'   => $_SERVER["DOCUMENT_ROOT"] . BX_ROOT . '/cache/maxposter/',
));
// Запрос к сервису / кешу
$client->setRequestThemeName('marks');

$domXml = $client->getXml()->saveXML();
// при ошибке запроса
if ($client->getResponseThemeName() == 'error') {
    CHTTP::SetStatus("404 Not Found");
    // TODO: получать message из ответа сервера
    ShowError(GetMessage('MAX_NOT_FOUND'));
    return;
}

if (mb_strtolower(SITE_CHARSET) != 'utf-8') {
    $data = iconv('utf-8', SITE_CHARSET, $domXml);
} else {
    $data = $domXml;
}
$xml = new CDataXML();
// Лучше бы через
// $xml->Load('/path/to/file');
$xml->LoadString($data);

/* @var $sf CDataXMLNode */
$arResult['FORM'] = array(); //инициализация пустого массива


$sf = $xml->SelectNodes('/response/marks');


$cars['marks'] = array(['mark'=>['id' => -1, 'title' => 'Выберите марку']]);
$cars['gears'] = array(
    ['value' => 1, 'title' => 'КПП'], ['value' => 'automatic', 'title' => 'Автоматическая'],
    ['value' => 'manual', 'title' => 'Ручная'], ['value' => 'variator', 'title' => 'Вариатор'],
    ['value' => 'robotized', 'title' => 'Робот']
    );

$cars['yearsFrom'] = array(['id' => 1930, 'title' => 'Год от']);
$cars['yearsTo'] = array(['id' => 2018, 'title' => 'Год до']);

for ($i=2018; $i > 1930; $i--) { 
    array_push($cars['yearsTo'], ['id' => $i, 'title' => $i]);
    array_push($cars['yearsFrom'], ['id' => $i, 'title' => $i]); 
}


$cars['priceFrom'] = array(['id' => 1, 'title' => 'Цена от']);
$cars['priceTo'] = array(['id' => 1, 'title' => 'Цена до']);

if(empty($filterParams['model_id'])) {
    $cars['selectedModel'] = -1;
} else {
    $cars['selectedModel'] = $filterParams['model_id'];
}

if(empty($filterParams['mark_id'])) {
    $cars['selectedMark'] = -1;
} else {
    $cars['selectedMark'] = $filterParams['mark_id'];
}

$cars['selectedGear'] = 1;
$cars['selectedYearFrom'] = 1930;
$cars['selectedYearTo'] = 2018;
$cars['selectedPriceFrom'] = 10000;
$cars['selectedPriceTo'] = 1000000;

if(empty($filterParams['gearbox_type'])) {
    $cars['selectedGear'] = 1;
} else {
    $cars['selectedGear'] = $filterParams['gearbox_type'];
}

if(empty($filterParams["year"]["from"])) {
    $cars['selectedYearFrom'] = 1930;
} else {
    $cars['selectedYearFrom'] = $filterParams["year"]["from"];
}

if(empty($filterParams["year"]["to"])) {
    $cars['selectedYearTo'] = 2018;
} else {
    $cars['selectedYearTo'] = $filterParams["year"]["to"];
}


if(empty($filterParams["price"]["to"])) {
    $cars['selectedPriceTo'] = '';
} else {
    $cars['selectedPriceTo'] = $filterParams["price"]["to"];
}

if(empty($filterParams["price"]["from"])) {
    $cars['selectedPriceFrom'] = '';
} else {
    $cars['selectedPriceFrom'] = $filterParams["price"]["from"];
}


$cars['models'] = [['id' => -1, 'title' => 'Выберите модель']];

if ($sf) {
    $sfFields = $sf->children();

    foreach ($sfFields as $sfField) {
        $node = $sfField->__toArray();
        
        $mark = [];

        $mark['mark']['id'] = $node['@']['mark_id'];
        $mark['mark']['title'] = $node['#']['name'][0]['#'];

        $mark['models'] = [['id' => -1, 'title' => 'Выберите модель']];

        foreach ($node['#']['models'][0]['#']['model'] as $model) {
            $m['id'] = $model['@']['model_id'];
            $m['title'] = $model['#']['name'][0]['#'];

            $mark['models'][] = $m;
            
        }
        array_push($cars['marks'], $mark);
    }
}

$arResult['URL'] = CComponentEngine::MakePathFromTemplate($arParams['URL_TEMPLATES_INDEX'], array());
$arResult['SELECTED'] = $filterParams;
$arResult['AUT'] = $cars;

// var_dump($arResult['SELECTED']);

$this->IncludeComponentTemplate();
